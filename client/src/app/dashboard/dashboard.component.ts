import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginAwareComponent } from '../shared/login-aware-component';
import { LoopBackAuth } from '../shared/sdk/services';
import { SocketService, SocketMessage } from '../shared/socket.service';
import { Subscription } from 'rxjs/Subscription';
import {Organization} from '../shared/sdk/models/Organization';
import {OrganizationApi} from '../shared/sdk/services/custom/Organization';
import {CustomUserApi} from '../shared/sdk/services/custom/CustomUser';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends LoginAwareComponent implements OnInit, OnDestroy {
	private socketSubscription: Subscription;

	organizations: Organization[];
	organization: Organization;

	constructor(auth: LoopBackAuth, socket: SocketService, private orgApi: OrganizationApi, private userApi: CustomUserApi) {
		super(auth);

		this.socketSubscription = socket.watchEvent('stuff').subscribe((message: SocketMessage) => {
			console.log('got message from socket', message);
		});
	}

	ngOnInit() {
		if (this.isLoggedIn()) {
			const user = this.auth.getCurrentUserData();

			// this.userApi.getO
			if (user.organizations) {
				console.log('org', user.organizations);
				this.organizations = typeof user.organizations === 'string' ? JSON.parse(user.organizations) : user.organizations;
				// this.orgApi.find({where: {id: {inq: user.organizations.map(o => o.id)}}}).subscribe((os: Organization[]) => {
				// 	this.organizations = os;
				// });
			}

			if (user && user.orgId) {
				this.orgApi.findById(user.orgId).subscribe((o: Organization) => this.organization = o);
			}
		}
	}

	loginToOrg(org) {
		const user = this.auth.getCurrentUserData();
		user.orgId = org.id;

		this.auth.setUser(user);
		this.auth.save();
	}

	ngOnDestroy() {
		if (this.socketSubscription) {
			this.socketSubscription.unsubscribe();
		}
	}

}
