import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {LoginAwareComponent} from './shared/login-aware-component';
import {FlashMessageService} from './flash-message/flash-message.service';
import { CustomUserApi, OrganizationApi, LoopBackAuth } from './shared/sdk/services';
import {CustomUser, Organization} from './shared/sdk/models';
import {PageTitleService} from './page-title/page-title.service';
import {HeaderService} from './header/header.service';
import {Observable} from 'rxjs/Observable';
import {CampaignCreateComponent} from './organization/campaign-create/campaign-create.component';
import {LoginComponent} from './user/login/login.component';
import {CreateAccountComponent} from './user/create-account/create-account.component';
import {LoopBackConfig} from './shared/sdk';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/do';
import {CurrentOrganizationService} from './shared/current-item.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent extends LoginAwareComponent implements OnInit {
	title = 'app works!';
	pageTitle = '';
	isCollapsed = true;

	apiUrl = `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}`;

	organization = new Organization();
	organizations: Organization[] = [];
	userInfo = new CustomUser();
	private organizationId: string;

	showSetup = false;
	showAdmin = false;
	sideBarIsClosed = true;
	showHeader = true;
	isOnClientLandingPage = false;
	isOnAccountLoginCreatePage = false;

	exportUrl = '';

	isSubscriptionActive = false;
	showSubscriptionBanner = false;
	constructor(private router: Router, private titleService: PageTitleService, private flash: FlashMessageService,
				private headerService: HeaderService, private activatedRoute: ActivatedRoute, private curOrgSrv: CurrentOrganizationService,
				private orgApi: OrganizationApi, private userApi: CustomUserApi, auth: LoopBackAuth) {
		super(auth);
	}

	ngOnInit() {
		this.titleService.getTitle().subscribe(title => {
			setTimeout(() => {
				this.pageTitle = title;
			});
		});
		this.headerService.getHeader().subscribe(header => {
			this.showHeader = header;
		});

		//watch the url changes to determine if we are on specific pages
		const routerEvent$ = this.router.events.map((curUrlEvent: any) => {
				try {
					//if they have the state property, we should be able to tell which component is in use
					if (curUrlEvent.state && curUrlEvent.state._root.children.length) {
						const component = curUrlEvent.state._root.children[0].value.component;
						this.isOnClientLandingPage = component === CampaignCreateComponent;
						this.isOnAccountLoginCreatePage = component === LoginComponent || component === CreateAccountComponent;
					}
				}catch (e) {
					//This shouldn't happen, but in case it's possible for one of those items to not exist.
					console.log('oopps!!', e);
				}

				return curUrlEvent.url;
			});

		//set up a stream that watches the current organization and re-initializes the local values if it changes
		const currentOrg$ = this.curOrgSrv.getCurrentItem().map(org => {
			console.log('just got a new org', org);

			//handle the situation that this is a new login
			if (!this.organizationId) {
				const curUser: CustomUser = this.auth.getCurrentUserData();
				//if the user now and exists and has orgs, then save those
				if (curUser && curUser.organizations) {
					this.organizations = curUser.organizations;
				}
			}

			//set the new organization
			this.organization = org;

			return org;
		});

		const hideBannerRegex = /\/(create|setup|configure-response|subscription)/;

		//Update to reload organizations when orgId or page url changes
		Observable.combineLatest(
			//watch the current organization and page urls
			currentOrg$,
			routerEvent$
		).subscribe(([org, curUrl]: [Organization, string]) => {

			// console.log('got event', curUrlEvent, org.subscriptionRenewalDate, org.id);
			//TODO check if the subscription is active. If they haven't set daysValid (or something), then they haven't done setup yet.
			//if they have an affiliate token, then treat their subscription as valid
			//otherwise, make sure that they subscriptionRenewalDate is in the future
			this.isSubscriptionActive = !!org.affiliateToken || new Date(org.subscriptionRenewalDate) > new Date();

			this.showSubscriptionBanner = (!this.isSubscriptionActive && !hideBannerRegex.test(curUrl));

			//set up the export header
			this.exportUrl = `${this.apiUrl}/Organizations/${org.id}/export-emails.xlsx?access_token=${this.auth.getAccessTokenId()}`;
		});

		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) return;

		console.log('user', curUser);
		this.organizationId = curUser.orgId;
		this.organizations = curUser.organizations;

		this.curOrgSrv.setCurrentItem(this.organizationId);
	}

	logout() {
		this.userApi.logout().subscribe(val => {
			this.flash.showMessage({message: 'Logged out successfully', messageClass: 'success'});
			this.router.navigateByUrl('/');
		}, err => {
			this.auth.clear();
			this.router.navigateByUrl('/');
		});
	}

	isHomePage() {
		return this.router.url === '/';
	}

	isRedeemPage() {
		return (this.router.url).includes('/redeem');
	}

	scrollToHash(hash) {
		const target = document.getElementById(hash);

		target.scrollIntoView({behavior: 'smooth'});
	}

	changeOrg(orgId) {
		// Update organization object
		this.orgApi.findById(orgId).subscribe((org: Organization) => {
			this.curOrgSrv.setCurrentItemObj(org);

			// Update token
			const user = this.auth.getCurrentUserData();
			user.orgId = orgId;
			this.auth.setUser(user);
			this.auth.save();

			// Navigate to new Home page
			this.router.navigateByUrl('/home/' + orgId);

			console.log('Switched Org to: ', orgId);

			console.log('Updated user? ', this.auth.getCurrentUserData());

			console.log('Updated organization? ', this.organization);
		});
	}
}
