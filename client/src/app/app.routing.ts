///<reference path="organization/closed-report/closed-report.component.ts"/>
import {ProfileComponent} from './user/profile/profile.component';
import {LoginComponent} from './user/login/login.component';
import {AdminComponent} from './admin/admin.component';
import {UserListComponent} from './admin/user-list/user-list.component';
import {CreateAccountComponent} from './user/create-account/create-account.component';
import {EditUsersComponent} from './user/edit-users/edit-users.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { LoggedInGuard } from './guards/login';
import { SuperuserGuard } from './guards/superuser';
import {WebsiteComponent} from './Website/website.component';
import {CampaignCreateComponent} from './organization/campaign-create/campaign-create.component';
import { ResponseConfigureComponent } from './organization/response-configure/response-configure.component';
import {
	CampaignResponseComponent,
	CompletedCampaignResponseComponent
} from './organization/campaign-response/campaign-response.component';
import {OrganizationDashboardComponent} from './organization/organization-dashboard/organization-dashboard.component';
import {OrgConfigureComponent} from './organization/org-configure/config.landingpage.component';
import {HomePageComponent} from './organization/HomePage/HomePage.component';
import {OrgSetupComponent} from './organization/Organization-Setup/organization-setup.component';
import {CampaignReportingComponent} from './organization/Reporting/reporting.component';
import {CampaignRequestComponent} from './organization/campaign-request/campaign-request.component';
import {CampaignRedemptionComponent} from './organization/campaign-redemption/campaign-redemption.component';
import {CustomizedComponent} from './organization/Customized/customized.component';
import {ClosedReportComponent} from './organization/closed-report/closed-report.component';
import {ExpiredCampaignComponent} from './organization/expired-campaigns/expired-campaigns.component';
import {DbComponent} from './organization/db/db.component';
import {SubscriptionComponent} from './organization/subscription/subscription.component';
import {ValueCalculatorComponent} from './value-calculator/value-calculator.component';

import { ContactComponent } from './contact/contact.component';
import {OfferOptionsComponent} from './organization/settings/offer-options/offer-options.component';
import {CampaignDaysComponent} from './organization/settings/campaign-days/campaign-days.component';
import {NotificationScheduleComponent} from './organization/settings/notification-schedule/notification-schedule.component';
import {ResponseConfigurationComponent} from './organization/settings/response-configuration/response-configuration.component';
import {CouponTimesComponent} from './organization/settings/coupon-times/coupon-times.component';
import {DefaultEmailComponent} from './organization/settings/default-email/default-email.component';
import {TermsConditionsComponent} from './organization/settings/terms-conditions/terms-conditions.component';
import {SocialMediaComponent} from './organization/settings/social-media/social-media.component';
import {DefaultEmailSupporterComponent} from './organization/settings/default-email-supporter/default-email-supporter.component';
import { TermsComponent } from './terms/terms.component';
import {AdminDashboardComponent} from './admin/dashboard/dashboard.component';

export const appRoutes: Routes = [
	{
		path: 'terms',
		component: TermsComponent,
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		pathMatch: 'full'
	},
	{
		path: 'db/:orgId',
		canActivate: [LoggedInGuard],
		component: DbComponent,
		pathMatch: 'full'
	},
	{
		path: '',
		component: WebsiteComponent,
		pathMatch: 'full'
	},
	{
		path: 'home/:orgId',
		canActivate: [LoggedInGuard],
		component: HomePageComponent,
		pathMatch: 'full'
	},
	{
		path: 'subscription/:orgId',
		canActivate: [LoggedInGuard],
		component: SubscriptionComponent,
		pathMatch: 'full'
	},
	{
		path: 'user-edit/:orgId',
		canActivate: [LoggedInGuard],
		component: EditUsersComponent,
	},
	{
		path: 'calculator',
		component: ValueCalculatorComponent
	},
	{
		path: 'contact',
		component: ContactComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'expired',
		component: ExpiredCampaignComponent
	},
	{
		path: 'profile',
		component: ProfileComponent,
		canActivate: [LoggedInGuard]
	},
	{
		path: 'admin',
		component: AdminComponent,
		canActivate: [LoggedInGuard, SuperuserGuard],
		children: [
			{
				path: 'users',
				component: UserListComponent
			},
			{
				path: 'users/:id',
				component: ProfileComponent,
				canActivate: [LoggedInGuard]
			},
			{
				path: 'dashboard',
				component: AdminDashboardComponent
			},
		]
	},
	{
		path: 'configure/:orgId',
		canActivate: [LoggedInGuard],
		component: OrgConfigureComponent,
	},
	{
		path: 'setup/:orgId',
		canActivate: [LoggedInGuard],
		component: OrgSetupComponent,
		children: [
			{
				path: 'offer-options',
				component: OfferOptionsComponent
			},
			{
				path: 'campaign-days',
				component: CampaignDaysComponent
			},
			{
				path: 'notification-schedule',
				component: NotificationScheduleComponent
			},
			{
				path: 'coupon-times',
				component: CouponTimesComponent
			},
			{
				path: 'default-email',
				component: DefaultEmailComponent
			},
			{
				path: 'default-email-supporter',
				component: DefaultEmailSupporterComponent
			},
			{
				path: 'terms-conditions',
				component: TermsConditionsComponent
			},
			{
				path: 'social-media',
				component: SocialMediaComponent
			},
			{
				path: 'response-configuration',
				component: ResponseConfigurationComponent
			},
		]
	},
	{
		path: 'Reporting/:orgId',
		canActivate: [LoggedInGuard],
		component: CampaignReportingComponent,
	},
	{
		path: 'Expired/:orgId',
		canActivate: [LoggedInGuard],
		component: ClosedReportComponent,
	},
	{
		path: 'configure-response/:orgId',
		canActivate: [LoggedInGuard],
		component: ResponseConfigureComponent,
	},
	{
		path: 'create',
		component: CreateAccountComponent,
	},
	{
		path: 'respond',
		component: CampaignResponseComponent,
	},
	{
		path: 'respond/new',
		component: CampaignResponseComponent,
	},
	{
		path: 'respond/read',
		component: CampaignResponseComponent,
		data: {status: 'read'}
	},
	{
		path: 'respond/completed',
		component: CompletedCampaignResponseComponent,
	},
	{
		path: 'dashboard/:orgId',
		canActivate: [LoggedInGuard],
		component: OrganizationDashboardComponent,
	},
	{
		path: 'campaigns/:campaignId',
		component: CampaignRequestComponent,
	},
	{
		path: 'redeem/:CampaignDetailsId',
		component: CampaignRedemptionComponent,
	},
	{
		path: ':campaign',
		component: CampaignCreateComponent,
	},
	{
		path: 'Customized/:orgId',
		canActivate: [LoggedInGuard],
		component: CustomizedComponent,
	},
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
