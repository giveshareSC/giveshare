export function exportCsv(documentElement) {
	console.log('elem', documentElement, Object.getPrototypeOf(documentElement));
	//construct a CSV string from the current table on screen
	const table = (documentElement instanceof HTMLTableElement ?  documentElement : documentElement.nativeElement);
	const rows = Array.from(table.rows || []).map((r: any) => {
		return '"' + Array.from(r.cells || [])
			.filter((c: HTMLTableCellElement) => !c.classList.contains('hide-from-export'))
			.map((c: any) => {
				return (c.innerText || '').replace('"', '""');
			})
			.join('","') + '"';
	});

	//Now that the CSV is constructed, start a download
	const csvData = new Blob([rows.join('\n')], {type: 'text/csv;charset=utf-8;'});
	//In FF link must be added to DOM to be clicked
	const link = document.createElement('a');
	link.href = window.URL.createObjectURL(csvData);
	link.setAttribute('download', 'export.csv');
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
	window.URL.revokeObjectURL(link.href);
}
