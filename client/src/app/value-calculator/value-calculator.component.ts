import {Component, OnInit} from '@angular/core';
import {FlashMessageService} from '../flash-message/flash-message.service';

class CalcPromptAnswers {
	requests?: number;
	grants?: number;
	giftcards?: number;
	avgCover?: number;

	isValid() {
		return typeof this.requests === 'number' &&
			typeof this.grants === 'number' &&
			typeof this.giftcards === 'number' &&
			typeof this.avgCover === 'number' &&
			this.requests >= 0 &&
			this.grants >= 0 && this.grants <= 100 &&
			this.giftcards >= 0 &&
			this.avgCover >= 0;
	}
}

@Component({
	selector: 'app-value-calculator',
	templateUrl: './value-calculator.component.html',
	styleUrls: ['./value-calculator.component.css']
})
export class ValueCalculatorComponent implements OnInit {

	readonly NEW_EMAILS_PER_CAMPAIGN = 23;
	readonly PERSON_PER_CAMPAIGN = [2,4,2];
	readonly PERCENTAGE_OF_COGS = [0.4,0.1,0.1];
	readonly PERCENTAGE_OF_NEW_GUESTS = 0.25;
	readonly PERCENTAGE_OF_SOCIAL_POSTS = 0.5;
	readonly SOCIAL_BLAST_COST = 3;
	readonly SOMETHING = 0.5; //TODO WHAT IS THIS????
	readonly VALUE_OF_EMAIL = [0.87, 0.87, 0.87];
	readonly VALUE_OF_NEW_GUEST = 15;
	readonly VALUE_OF_POSITIVE_POST = [13.46,13.46,13.46];

	showAnswers = false;

	answers = new CalcPromptAnswers();
	business: string;

	valueOfNoNo: number;
	giftcardSavings: number;
	valueOfEmailCapture: number;
	newBusiness: number;
	freeGoodwill: number;
	newCustomerAcquisition: number;
	totalValue: number;

	showGiftcardCalc = false;
	showNewBusinessCalc = false;
	showValueOfNoNoCalc = false;
	showEmailCalc = false;
	showFreeGoodwillCalc = false;
	showNewCustCalc = false;

	constructor(private flashMessage: FlashMessageService) {
	}

	ngOnInit() {
	}

	calculate() {
		console.log('calc called', this.answers);
		console.log("Business:", this.business);

		var businessInt = parseInt(this.business);

		if(businessInt >=0 && businessInt <= 2 && this.answers.isValid()) {

			const perGrants = 0.01 * this.answers.grants;
			console.log('percentage', perGrants);
			this.valueOfNoNo = this.answers.requests * (1 - perGrants);

			this.giftcardSavings = this.answers.requests * perGrants * this.answers.giftcards * this.PERCENTAGE_OF_COGS[businessInt];

			this.valueOfEmailCapture = this.answers.requests * this.NEW_EMAILS_PER_CAMPAIGN * this.VALUE_OF_EMAIL[businessInt];

			this.newBusiness = this.answers.requests * perGrants * this.answers.avgCover * this.PERSON_PER_CAMPAIGN[businessInt];

			this.freeGoodwill = this.answers.requests * perGrants * this.VALUE_OF_POSITIVE_POST[businessInt];

			this.newCustomerAcquisition = (businessInt == 1 ? this.answers.requests * perGrants * 225 : this.answers.avgCover * 4);

			this.totalValue = this.valueOfNoNo + this.giftcardSavings + this.valueOfEmailCapture +
				this.newBusiness + this.freeGoodwill + this.newCustomerAcquisition;

			this.showAnswers = true;
		} else {
			this.flashMessage.showError('Please provide valid answers for all of the questions');
			console.log('answers are not valid');
		}
	}
}
