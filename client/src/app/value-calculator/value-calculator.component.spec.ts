import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueCalculatorComponent } from './value-calculator.component';

describe('ValueCalculatorComponent', () => {
  let component: ValueCalculatorComponent;
  let fixture: ComponentFixture<ValueCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
