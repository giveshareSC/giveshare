import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { CustomUserApi } from '../shared/sdk/services/custom';
import {LoginPageServiceService} from "../login-page-service/login-page-service.service";

@Injectable()
export class LoggedInGuard implements CanActivate {
	constructor(private userApi: CustomUserApi, private router: Router, private pageService: LoginPageServiceService) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (this.userApi.getCurrentId() != null) {
			return true;
		} else {
			// console.log('sdfsdf', route);
			this.pageService.setPage('/' + route.url.join('/'));
			this.router.navigateByUrl('/login');
			return false;
		}
	}
}
