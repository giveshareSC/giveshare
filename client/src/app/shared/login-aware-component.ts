import { LoopBackAuth } from './sdk/services';
import { CustomUser } from './sdk/models';

export class LoginAwareComponent {
	constructor(protected auth: LoopBackAuth) { }

	isLoggedIn () {
		return this.auth.getCurrentUserId() != null;
	}

	isSuperuser () {
		const curUser: CustomUser = this.auth.getCurrentUserData();
		return curUser && curUser.roles && curUser.roles.find(r => r.name === 'admin');
	}

	getUserOrgId () {
		const curUser: CustomUser = this.auth.getCurrentUserData();
		return curUser && curUser.orgId;
	}

	getUserDisplayName () {
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (curUser) {
			return `${curUser.firstName || ''} ${curUser.lastName || ''}`;
		} else {
			return '';
		}
	}
}
