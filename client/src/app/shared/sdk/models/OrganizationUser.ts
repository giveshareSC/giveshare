/* tslint:disable */
import {
  CustomUser,
  Organization
} from '../index';

declare var Object: any;
export interface OrganizationUserInterface {
  "organizationId": any;
  "userId": any;
  "id"?: any;
  customUser?: CustomUser;
  organization?: Organization;
}

export class OrganizationUser implements OrganizationUserInterface {
  "organizationId": any;
  "userId": any;
  "id": any;
  customUser: CustomUser;
  organization: Organization;
  constructor(data?: OrganizationUserInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `OrganizationUser`.
   */
  public static getModelName() {
    return "OrganizationUser";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of OrganizationUser for dynamic purposes.
  **/
  public static factory(data: OrganizationUserInterface): OrganizationUser{
    return new OrganizationUser(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'OrganizationUser',
      plural: 'OrganizationUsers',
      properties: {
        "organizationId": {
          name: 'organizationId',
          type: 'any'
        },
        "userId": {
          name: 'userId',
          type: 'any'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        customUser: {
          name: 'customUser',
          type: 'CustomUser',
          model: 'CustomUser'
        },
        organization: {
          name: 'organization',
          type: 'Organization',
          model: 'Organization'
        },
      }
    }
  }
}
