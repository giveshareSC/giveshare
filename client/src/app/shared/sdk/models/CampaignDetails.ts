/* tslint:disable */
import {
  Campaign,
  Organization
} from '../index';

declare var Object: any;
export interface CampaignDetailsInterface {
  "CampaignId": any;
  "Email": string;
  "OrgId": any;
  "RequestDate"?: Date;
  "OpenDate"?: Date;
  "RedeemedDate"?: Date;
  "TimeRedeemed"?: string;
  "Status"?: string;
  "TotalSales"?: number;
  "TotalGuests"?: number;
  "id"?: any;
  donationAmt?: number;
  campaign?: Campaign;
  organization?: Organization;
  redeemingOrg?: Organization;
}

export class CampaignDetails implements CampaignDetailsInterface {
  "CampaignId": any;
  "Email": string;
  "OrgId": any;
  "RequestDate": Date;
  "OpenDate": Date;
  "RedeemedDate": Date;
  "TimeRedeemed": string;
  "Status": string;
  "TotalSales": number;
  "TotalGuests": number;
  "id": any;
  donationAmt?: number;
  campaign: Campaign;
  organization: Organization;
  redeemingOrg?: Organization;
	constructor(data?: CampaignDetailsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `CampaignDetails`.
   */
  public static getModelName() {
    return "CampaignDetails";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of CampaignDetails for dynamic purposes.
  **/
  public static factory(data: CampaignDetailsInterface): CampaignDetails{
    return new CampaignDetails(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'CampaignDetails',
      plural: 'CampaignDetails',
      properties: {
        "CampaignId": {
          name: 'CampaignId',
          type: 'any'
        },
        "Email": {
          name: 'Email',
          type: 'string'
        },
        "OrgId": {
          name: 'OrgId',
          type: 'any'
        },
        "RequestDate": {
          name: 'RequestDate',
          type: 'Date'
        },
        "OpenDate": {
          name: 'OpenDate',
          type: 'Date'
        },
        "RedeemedDate": {
          name: 'RedeemedDate',
          type: 'Date'
        },
        "TimeRedeemed": {
          name: 'TimeRedeemed',
          type: 'string'
        },
        "Status": {
          name: 'Status',
          type: 'string'
        },
        "TotalSales": {
          name: 'TotalSales',
          type: 'number'
        },
        "TotalGuests": {
          name: 'TotalGuests',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        campaign: {
          name: 'campaign',
          type: 'Campaign',
          model: 'Campaign'
        },
        organization: {
          name: 'organization',
          type: 'Organization',
          model: 'Organization'
        },
      }
    }
  }
}
