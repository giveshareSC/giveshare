/* tslint:disable */

declare var Object: any;
export interface OrganizationCouponOptionInterface {
  "organizationId": any;
  "discountAsPercent": boolean;
  "discountAmount": number;
  "id"?: any;
}

export class OrganizationCouponOption implements OrganizationCouponOptionInterface {
  "organizationId": any;
  "discountAsPercent": boolean;
  "discountAmount": number;
  "id": any;
  constructor(data?: OrganizationCouponOptionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `OrganizationCouponOption`.
   */
  public static getModelName() {
    return "OrganizationCouponOption";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of OrganizationCouponOption for dynamic purposes.
  **/
  public static factory(data: OrganizationCouponOptionInterface): OrganizationCouponOption{
    return new OrganizationCouponOption(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'OrganizationCouponOption',
      plural: 'OrganizationCouponOptions',
      properties: {
        "organizationId": {
          name: 'organizationId',
          type: 'any'
        },
        "discountAsPercent": {
          name: 'discountAsPercent',
          type: 'boolean'
        },
        "discountAmount": {
          name: 'discountAmount',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
