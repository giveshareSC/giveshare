/* tslint:disable */

declare var Object: any;
export interface EmailTemplateInterface {
  "organizationId": any;
  "name": string;
  "message": string;
  "id"?: any;
}

export class EmailTemplate implements EmailTemplateInterface {
  "organizationId": any;
  "name": string;
  "message": string;
  "id": any;
  constructor(data?: EmailTemplateInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `EmailTemplate`.
   */
  public static getModelName() {
    return "EmailTemplate";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of EmailTemplate for dynamic purposes.
  **/
  public static factory(data: EmailTemplateInterface): EmailTemplate{
    return new EmailTemplate(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'EmailTemplate',
      plural: 'EmailTemplates',
      properties: {
        "organizationId": {
          name: 'organizationId',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "message": {
          name: 'message',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
      }
    }
  }
}
