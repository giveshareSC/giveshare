/* tslint:disable */
import {
  Campaign,
  CustomUser,
  OrganizationUser
} from '../index';

declare var Object: any;
export interface OrganizationInterface {
  "id"?: any;
  "name": string;
  "slug"?: string;
  "logo"?: string;
  "website"?: string;
  "address": string;
  "city": string;
  "state": string;
  "zip": string;
  "phoneNumber"?: string;
  "createDate"?: Date;
  "subscriptionRenewalDate"?: Date;
  "discountAmount"?: number;
  "mgrOverRide": number;
  "redemptionCode": number;
  "threshHold": number;
  "allowCustom"?: boolean;
  "specifyHours"?: boolean;
  "Sun1FromTime"?: string;
  "Sun1ToTime"?: string;
  "Sun2FromTime"?: string;
  "Sun2ToTime"?: string;
  "Mon1FromTime"?: string;
  "Mon1ToTime"?: string;
  "Mon2FromTime"?: string;
  "Mon2ToTime"?: string;
  "Tue1FromTime"?: string;
  "Tue1ToTime"?: string;
  "Tue2FromTime"?: string;
  "Tue2ToTime"?: string;
  "Wed1FromTime"?: string;
  "Wed1ToTime"?: string;
  "Wed2FromTime"?: string;
  "Wed2ToTime"?: string;
  "Thur1FromTime"?: string;
  "Thur1ToTime"?: string;
  "Thur2FromTime"?: string;
  "Thur2ToTime"?: string;
  "Fri1FromTime"?: string;
  "Fri1ToTime"?: string;
  "Fri2FromTime"?: string;
  "Fri2ToTime"?: string;
  "Sat1FromTime"?: string;
  "Sat1ToTime"?: string;
  "Sat2FromTime"?: string;
  "Sat2ToTime"?: string;
  "SunNew"?: boolean;
  "SunClosed"?: boolean;
  "MonNew"?: boolean;
  "MonClosed"?: boolean;
  "TueNew"?: boolean;
  "TueClosed"?: boolean;
  "WedNew"?: boolean;
  "WedClosed"?: boolean;
  "ThurNew"?: boolean;
  "ThurClosed"?: boolean;
  "FriNew"?: boolean;
  "FriClosed"?: boolean;
  "SatNew"?: boolean;
  "SatClosed"?: boolean;
  "weeks"?: boolean;
  "days"?: boolean;
  "daysValid"?: number;
  "defEmailContent"?: string;
  "defEmailContentInvite"?: string;
  "defOfferContent"?: string;
  "termsAndCond"?: string;
  "offerTC"?: string;
  "setupComplete"?: boolean;
  "allowMultipleRedeems"?: boolean;
  couponOptions?: any[];
  campaigns?: Campaign[];
  emailTemplates?: any[];
  customUsers?: CustomUser[];
  organizationUsers?: OrganizationUser[];
  campaignDetails?: any[];
  affiliateToken?: string;
}

export class Organization implements OrganizationInterface {
  "id": any;
  "name": string;
  "slug": string;
  "logo": string;
  "website": string;
  "address": string;
  "city": string;
  "state": string;
  "zip": string;
  "phoneNumber": string;
  "createDate": Date;
  "subscriptionRenewalDate": Date;
  "discountAmount": number;
  "mgrOverRide": number;
  "redemptionCode": number;
  "threshHold": number;
  "allowCustom": boolean;
  "specifyHours": boolean;
  "Sun1FromTime": string;
  "Sun1ToTime": string;
  "Sun2FromTime": string;
  "Sun2ToTime": string;
  "Mon1FromTime": string;
  "Mon1ToTime": string;
  "Mon2FromTime": string;
  "Mon2ToTime": string;
  "Tue1FromTime": string;
  "Tue1ToTime": string;
  "Tue2FromTime": string;
  "Tue2ToTime": string;
  "Wed1FromTime": string;
  "Wed1ToTime": string;
  "Wed2FromTime": string;
  "Wed2ToTime": string;
  "Thur1FromTime": string;
  "Thur1ToTime": string;
  "Thur2FromTime": string;
  "Thur2ToTime": string;
  "Fri1FromTime": string;
  "Fri1ToTime": string;
  "Fri2FromTime": string;
  "Fri2ToTime": string;
  "Sat1FromTime": string;
  "Sat1ToTime": string;
  "Sat2FromTime": string;
  "Sat2ToTime": string;
  "SunNew": boolean;
  "SunClosed": boolean;
  "MonNew": boolean;
  "MonClosed": boolean;
  "TueNew": boolean;
  "TueClosed": boolean;
  "WedNew": boolean;
  "WedClosed": boolean;
  "ThurNew": boolean;
  "ThurClosed": boolean;
  "FriNew": boolean;
  "FriClosed": boolean;
  "SatNew": boolean;
  "SatClosed": boolean;
  "weeks": boolean;
  "days": boolean;
  "daysValid": number;
  "defEmailContent": string;
  "defEmailContentInvite": string;
  "defOfferContent": string;
  "termsAndCond": string;
  "offerTC": string;
  "setupComplete": boolean;
  "allowMultipleRedeems": boolean;
  couponOptions: any[];
  campaigns: Campaign[];
  emailTemplates: any[];
  customUsers: CustomUser[];
  organizationUsers: OrganizationUser[];
  campaignDetails: any[];
  affiliateToken?: string;
  constructor(data?: OrganizationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Organization`.
   */
  public static getModelName() {
    return "Organization";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Organization for dynamic purposes.
  **/
  public static factory(data: OrganizationInterface): Organization{
    return new Organization(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Organization',
      plural: 'Organizations',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "slug": {
          name: 'slug',
          type: 'string'
        },
        "logo": {
          name: 'logo',
          type: 'string'
        },
        "website": {
          name: 'website',
          type: 'string'
        },
        "address": {
          name: 'address',
          type: 'string'
        },
        "city": {
          name: 'city',
          type: 'string'
        },
        "state": {
          name: 'state',
          type: 'string'
        },
        "zip": {
          name: 'zip',
          type: 'string'
        },
        "phoneNumber": {
          name: 'phoneNumber',
          type: 'string'
        },
        "createDate": {
          name: 'createDate',
          type: 'Date'
        },
        "subscriptionRenewalDate": {
          name: 'subscriptionRenewalDate',
          type: 'Date'
        },
        "discountAmount": {
          name: 'discountAmount',
          type: 'number'
        },
        "mgrOverRide": {
          name: 'mgrOverRide',
          type: 'number'
        },
        "redemptionCode": {
          name: 'redemptionCode',
          type: 'number'
        },
        "threshHold": {
          name: 'threshHold',
          type: 'number'
        },
        "allowCustom": {
          name: 'allowCustom',
          type: 'boolean'
        },
        "specifyHours": {
          name: 'specifyHours',
          type: 'boolean'
        },
        "Sun1FromTime": {
          name: 'Sun1FromTime',
          type: 'string'
        },
        "Sun1ToTime": {
          name: 'Sun1ToTime',
          type: 'string'
        },
        "Sun2FromTime": {
          name: 'Sun2FromTime',
          type: 'string'
        },
        "Sun2ToTime": {
          name: 'Sun2ToTime',
          type: 'string'
        },
        "Mon1FromTime": {
          name: 'Mon1FromTime',
          type: 'string'
        },
        "Mon1ToTime": {
          name: 'Mon1ToTime',
          type: 'string'
        },
        "Mon2FromTime": {
          name: 'Mon2FromTime',
          type: 'string'
        },
        "Mon2ToTime": {
          name: 'Mon2ToTime',
          type: 'string'
        },
        "Tue1FromTime": {
          name: 'Tue1FromTime',
          type: 'string'
        },
        "Tue1ToTime": {
          name: 'Tue1ToTime',
          type: 'string'
        },
        "Tue2FromTime": {
          name: 'Tue2FromTime',
          type: 'string'
        },
        "Tue2ToTime": {
          name: 'Tue2ToTime',
          type: 'string'
        },
        "Wed1FromTime": {
          name: 'Wed1FromTime',
          type: 'string'
        },
        "Wed1ToTime": {
          name: 'Wed1ToTime',
          type: 'string'
        },
        "Wed2FromTime": {
          name: 'Wed2FromTime',
          type: 'string'
        },
        "Wed2ToTime": {
          name: 'Wed2ToTime',
          type: 'string'
        },
        "Thur1FromTime": {
          name: 'Thur1FromTime',
          type: 'string'
        },
        "Thur1ToTime": {
          name: 'Thur1ToTime',
          type: 'string'
        },
        "Thur2FromTime": {
          name: 'Thur2FromTime',
          type: 'string'
        },
        "Thur2ToTime": {
          name: 'Thur2ToTime',
          type: 'string'
        },
        "Fri1FromTime": {
          name: 'Fri1FromTime',
          type: 'string'
        },
        "Fri1ToTime": {
          name: 'Fri1ToTime',
          type: 'string'
        },
        "Fri2FromTime": {
          name: 'Fri2FromTime',
          type: 'string'
        },
        "Fri2ToTime": {
          name: 'Fri2ToTime',
          type: 'string'
        },
        "Sat1FromTime": {
          name: 'Sat1FromTime',
          type: 'string'
        },
        "Sat1ToTime": {
          name: 'Sat1ToTime',
          type: 'string'
        },
        "Sat2FromTime": {
          name: 'Sat2FromTime',
          type: 'string'
        },
        "Sat2ToTime": {
          name: 'Sat2ToTime',
          type: 'string'
        },
        "SunNew": {
          name: 'SunNew',
          type: 'boolean'
        },
        "SunClosed": {
          name: 'SunClosed',
          type: 'boolean'
        },
        "MonNew": {
          name: 'MonNew',
          type: 'boolean'
        },
        "MonClosed": {
          name: 'MonClosed',
          type: 'boolean'
        },
        "TueNew": {
          name: 'TueNew',
          type: 'boolean'
        },
        "TueClosed": {
          name: 'TueClosed',
          type: 'boolean'
        },
        "WedNew": {
          name: 'WedNew',
          type: 'boolean'
        },
        "WedClosed": {
          name: 'WedClosed',
          type: 'boolean'
        },
        "ThurNew": {
          name: 'ThurNew',
          type: 'boolean'
        },
        "ThurClosed": {
          name: 'ThurClosed',
          type: 'boolean'
        },
        "FriNew": {
          name: 'FriNew',
          type: 'boolean'
        },
        "FriClosed": {
          name: 'FriClosed',
          type: 'boolean'
        },
        "SatNew": {
          name: 'SatNew',
          type: 'boolean'
        },
        "SatClosed": {
          name: 'SatClosed',
          type: 'boolean'
        },
        "weeks": {
          name: 'weeks',
          type: 'boolean'
        },
        "days": {
          name: 'days',
          type: 'boolean'
        },
        "daysValid": {
          name: 'daysValid',
          type: 'number'
        },
        "defEmailContent": {
          name: 'defEmailContent',
          type: 'string'
        },
        "defEmailContentInvite": {
          name: 'defEmailContentInvite',
          type: 'string'
        },
        "defOfferContent": {
          name: 'defOfferContent',
          type: 'string'
        },
        "termsAndCond": {
          name: 'termsAndCond',
          type: 'string'
        },
        "offerTC": {
          name: 'offerTC',
          type: 'string'
        },
        "setupComplete": {
          name: 'setupComplete',
          type: 'boolean',
          default: false
        },
        "allowMultipleRedeems": {
          name: 'allowMultipleRedeems',
          type: 'boolean',
          default: false
        },
        "affiliateToken": {
          name: 'affiliateToken',
          type: 'string'
        },
      },
      relations: {
        couponOptions: {
          name: 'couponOptions',
          type: 'any[]',
          model: ''
        },
        campaigns: {
          name: 'campaigns',
          type: 'Campaign[]',
          model: 'Campaign'
        },
        emailTemplates: {
          name: 'emailTemplates',
          type: 'any[]',
          model: ''
        },
        customUsers: {
          name: 'customUsers',
          type: 'CustomUser[]',
          model: 'CustomUser'
        },
        organizationUsers: {
          name: 'organizationUsers',
          type: 'OrganizationUser[]',
          model: 'OrganizationUser'
        },
        campaignDetails: {
          name: 'campaignDetails',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
