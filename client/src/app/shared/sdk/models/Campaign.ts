/* tslint:disable */
import {
  CampaignDetails
} from '../index';

declare var Object: any;
export interface CampaignInterface {
	"w9Reference": string;
  "pointOfContactEmail": string;
  "pointOfContactName": string;
  "organizationId": any;
  "charityName": string;
  "charityLogo"?: string;
  "startDate"?: Date;
  "reqDate"?: Date;
  "endDate"?: Date;
  "description": string;
  "urlLink"?: string;
  "status"?: string;
  "mailingAddress": string;
  "mailingCity": string;
  "mailingState": string;
  "mailingZip": string;
  "isRead"?: boolean;
  "resultingAmount"?: number;
  "discountPercentage"?: number;
  "id"?: any;
  organization?: any;
  details?: CampaignDetails[];
}

export class Campaign implements CampaignInterface {
	"w9Reference": string;
  "pointOfContactEmail": string;
  "pointOfContactName": string;
  "organizationId": any;
  "charityName": string;
  "charityLogo": string;
  "startDate": Date;
  "reqDate": Date;
  "endDate": Date;
  "description": string;
  "urlLink": string;
  "status": string;
  "mailingAddress": string;
  "mailingCity": string;
  "mailingState": string;
  "mailingZip": string;
  "isRead": boolean;
  "resultingAmount": number;
  "discountPercentage": number;
  "id": any;
  organization: any;
  details: CampaignDetails[];
  constructor(data?: CampaignInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Campaign`.
   */
  public static getModelName() {
    return "Campaign";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Campaign for dynamic purposes.
  **/
  public static factory(data: CampaignInterface): Campaign{
    return new Campaign(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Campaign',
      plural: 'Campaigns',
      properties: {
        "pointOfContactEmail": {
          name: 'pointOfContactEmail',
          type: 'string'
        },
        "pointOfContactName": {
          name: 'pointOfContactName',
          type: 'string'
        },
        "organizationId": {
          name: 'organizationId',
          type: 'any'
        },
        "charityName": {
          name: 'charityName',
          type: 'string'
        },
        "charityLogo": {
          name: 'charityLogo',
          type: 'string'
        },
        "startDate": {
          name: 'startDate',
          type: 'Date'
        },
        "reqDate": {
          name: 'reqDate',
          type: 'Date'
        },
        "endDate": {
          name: 'endDate',
          type: 'Date'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "urlLink": {
          name: 'urlLink',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "mailingAddress": {
          name: 'mailingAddress',
          type: 'string'
        },
        "mailingCity": {
          name: 'mailingCity',
          type: 'string'
        },
        "mailingState": {
          name: 'mailingState',
          type: 'string'
        },
        "mailingZip": {
          name: 'mailingZip',
          type: 'string'
        },
        "isRead": {
          name: 'isRead',
          type: 'boolean',
          default: false
        },
        "resultingAmount": {
          name: 'resultingAmount',
          type: 'number'
        },
        "discountPercentage": {
          name: 'discountPercentage',
          type: 'number'
        },
        "id": {
          name: 'id',
          type: 'any'
        },
      },
      relations: {
        organization: {
          name: 'organization',
          type: 'any',
          model: ''
        },
        details: {
          name: 'details',
          type: 'CampaignDetails[]',
          model: 'CampaignDetails'
        },
      }
    }
  }
}
