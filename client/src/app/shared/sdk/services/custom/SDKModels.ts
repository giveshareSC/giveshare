/* tslint:disable */
import { Injectable } from '@angular/core';
import { Email } from '../../models/Email';
import { RoleMapping } from '../../models/RoleMapping';
import { Role } from '../../models/Role';
import { CustomUser } from '../../models/CustomUser';
import { Organization } from '../../models/Organization';
import { OrganizationCouponOption } from '../../models/OrganizationCouponOption';
import { Campaign } from '../../models/Campaign';
import { EmailTemplate } from '../../models/EmailTemplate';
import { OrganizationUser } from '../../models/OrganizationUser';
import { CampaignDetails } from '../../models/CampaignDetails';
import { General } from '../../models/General';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Email: Email,
    RoleMapping: RoleMapping,
    Role: Role,
    CustomUser: CustomUser,
    Organization: Organization,
    OrganizationCouponOption: OrganizationCouponOption,
    Campaign: Campaign,
    EmailTemplate: EmailTemplate,
    OrganizationUser: OrganizationUser,
    CampaignDetails: CampaignDetails,
    General: General,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
