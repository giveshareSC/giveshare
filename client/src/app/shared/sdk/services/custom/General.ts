/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { JSONSearchParams } from '../core/search.params';
import { ErrorHandler } from '../core/error.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { General } from '../../models/General';


/**
 * Api services for the `General` model.
 */
@Injectable()
export class GeneralApi extends BaseLoopBackApi {

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  models, auth, searchParams, errorHandler);
  }

  /**
   * Sends a contact notification
   *
   * @param {object} data Request data.
   *
   *  - `contactName` – `{string}` - The name of the contact
   *
   *  - `contactEmail` – `{string}` - The email address of the contact
   *
   *  - `contactPhone` – `{string}` - The phone number of the contact
   *
   *  - `message` – `{string}` - The message that the user is trying to submit
   *
   *  - `captchaResponse` – `{string}` - The captchaResponse that is used to validate the request as being from a real person
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * Data properties:
   *
   *  - `success` – `{boolean}` - Whether or not the request was successful
   *
   *  - `message` – `{string}` - A message to the user giving an explanation
   */
  public addContactRequest(contactName: any, contactEmail: any, contactPhone: any = {}, message: any, captchaResponse: any): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/General/addContactRequest";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (contactName) _urlParams.contactName = contactName;
    if (contactEmail) _urlParams.contactEmail = contactEmail;
    if (contactPhone) _urlParams.contactPhone = contactPhone;
    if (message) _urlParams.message = message;
    if (captchaResponse) _urlParams.captchaResponse = captchaResponse;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `General`.
   */
  public getModelName() {
    return "General";
  }
}
