import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {BaseLoopBackApi} from './sdk/services/core';
import {LoopBackFilter, Organization} from './sdk/models';
import {OrganizationApi} from './sdk/services/custom';


export class CurrentItemService<T> {
	private currentItemSource = new Subject<T>();
	private curItem: T;
	private hasIdSet = false;

	constructor(private api: BaseLoopBackApi, private findFilter: LoopBackFilter) {}

	unsetCurrentItem() {
		this.hasIdSet = false;
		this.curItem = null;
	}

	setCurrentItem(objId: number|string) {
		this.hasIdSet = true;
		// console.log('making call', this.constructor.name);
		this.api.findById(objId, this.findFilter).subscribe((item: T) => {
			// console.log('received call response', item, this.constructor.name);
			this.curItem = item;
			this.currentItemSource.next(item);
		});
	}

	setCurrentItemObj(obj: T) {
		this.hasIdSet = true;

		//copy this object to make sure that no components can (un)intentionally mess with other by changing props on this value
		const objCopy = JSON.parse(JSON.stringify(obj)) as T;

		this.curItem = objCopy;
		this.currentItemSource.next(objCopy);
	}

	getCurrentItem(): Observable<T> {
		// console.log('request made', this.constructor.name, this.curItem);
		return Observable.create(observer => {
			//if we already have a valid object, add it to the stream
			if (this.curItem) {
				// console.log('I has thing', this.constructor.name);
				observer.next(this.curItem);
			}
			//watch the stream for updates
			const subsc = this.currentItemSource.subscribe(val => {
				// console.log('got another thing', this.constructor.name);
				observer.next(val);
			});

			//set up dispose
			return () => {
				if (subsc) {
					subsc.unsubscribe();
				}
			};
		});
	}

	hasCurrentItem() {
		return this.hasIdSet;
	}

}


@Injectable()
export class CurrentOrganizationService extends CurrentItemService<Organization> {
	constructor(orgApi: OrganizationApi) {
		super(orgApi, {});
	}
}

