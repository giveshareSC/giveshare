import { Component } from '@angular/core';
@Component({
	//selector: 'app-profile',
	template: `<div class='container'>
			<div class='row'>
				<div class="col-md-8">
					<h2>Administration</h2>
				</div>
				<nav class='col-md-4'>
					<a routerLink="users">Users</a>
					<a routerLink="dashboard">Executive Dashboard</a>
				</nav>
			</div>
			<div class='row'>
				<router-outlet></router-outlet>
			</div>
	</div>`,
	styles: [
		'nav a {margin: 15px; display: inline-block;}',
		'h2 {margin-top: 0}',
	]
})
export class AdminComponent {}
