import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {OrganizationApi} from '../../shared/sdk/services/custom';
import {Organization} from '../../shared/sdk/models';

import {exportCsv} from '../../utilities'

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.less']
})
export class AdminDashboardComponent implements OnInit {

	@ViewChild('summaryTable') summaryTable: ElementRef;
	@ViewChild('detailsTable') detailsTable: ElementRef;

	summaries: any[] = [];
	organizationStatus = 'active';
	currentOrg: Organization = null;

	constructor(private orgApi: OrganizationApi) {
	}

	ngOnInit() {
		this.loadSummaries();
	}

	loadSummaries() {
		const query = {} as any;
		switch (this.organizationStatus) {
			case 'active':
				query.subscriptionRenewalDate = {gte: new Date()};
				break;
			case 'inactive':
				query.or = [
					{subscriptionRenewalDate: null},
					{subscriptionRenewalDate: {lt: new Date()}}
				];
				break;
		}
		this.orgApi.organizationSummaries({where: query})
			.subscribe(summ => this.summaries = summ);
	}

	loadDetails(org: Organization) {
		if (org.campaigns && org.campaigns.length) {
			this.currentOrg = org;
		} else {
			this.orgApi.getCampaigns(org.id).subscribe(camps => {
				org.campaigns = camps;
				this.currentOrg = org;
			});
		}
	}

	exportData() {
		//construct a CSV string from the current table on screen
		exportCsv(this.currentOrg ? this.detailsTable : this.summaryTable);
	}
}
