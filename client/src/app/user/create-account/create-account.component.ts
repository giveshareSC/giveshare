import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CustomUserApi} from '../../shared/sdk/services/custom';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {OrganizationUserApi} from '../../shared/sdk/services/custom/OrganizationUser';
import {SDKToken} from '../../shared/sdk/models/BaseModels';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {PageTitleService} from '../../page-title/page-title.service';
import { LoginAwareComponent } from '../../shared/login-aware-component';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import {CurrentOrganizationService} from '../../shared/current-item.service';

declare const filestack: {
	init(apiKey: string): {
		pick({ maxFiles }: { maxFiles: number }):
			Promise<{ filesUploaded: { url: string }[] }>
	}
};

@Component({
	selector: 'app-create-account',
	templateUrl: './create-account.component.html',
	styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent extends LoginAwareComponent implements OnInit {

	userInfo = new CustomUser();
	organization = new Organization();
	emailError: string;
	subscriptionParam = '';

	constructor(private userApi: CustomUserApi, private router: Router, private route: ActivatedRoute, private orgUserApi: OrganizationUserApi,
				private flashMessageService: FlashMessageService, private orgApi: OrganizationApi, auth: LoopBackAuth,
				titleService: PageTitleService, private curOrgSrv: CurrentOrganizationService) {
		super(auth);
		titleService.setTitle('Create Account');
	}

	ngOnInit() {
	}

	createUser() {
		console.log(this.organization);
		this.organization.redemptionCode = 1234;
		this.organization.threshHold = 100;
		this.organization.mgrOverRide = 1234;
		this.organization.discountAmount = 10;
		this.organization.daysValid = 30;
		this.organization.specifyHours = true;
		this.organization.allowCustom = true;
		this.organization.offerTC = 'Offer valid Sun-Th excluding 11:30 - 1:30 TH and after 4:30 TH';
		this.organization.defEmailContentInvite = 'Thanks for helping ' + this.organization.name +
			' support {{charity-name}} ' +
			'through it’s dine to Donate program!! {{discount-percent}}' +
			'% of all sales redeemed with this invitation will be donated to ' +
			'{{charity-name}}!! We look forward to seeing you soon!!' +
			'Please save this email somewhere safe as you can redeem as often as you like during the event! ' +
			'And we have PLENTY of menu options for you to try!!';
		this.organization.defOfferContent = this.organization.name + ' is happy to announce that it is sponsoring {{charity-name}}' +
			' through it’s dine to Donate program!! ' +
			'{{discount-percent}} of all sales redeemed with this invitation will be donated to ' +
			'{{charity-name}}!!' +
			'Come early and often as {{restaurant-name}} will donate {{discount-percent}}% EACH time you come in! ' +
			'It’s like a rewards program for {{charity-name}}!!' +
			'See you soon! \n';
		this.organization.defEmailContent = this.organization.name +
			' is always happy to support {{charity-name}} through it’s dine to Donate program!!  We will be donating ' +
			'{{discount-percent}}% of all sales associated with this campaign during the specified times. ' +
			'Please find attached a link to a digital ' +
			'invitation that you can share with your email and social media network, and please encourage everyone to share it themselves. ' +
			'Please encourage your audience to join us as often as they want. ' +
			'Multiple redemptions is encouraged and we will donate {{discount-percent}}% EVERY time you come in until {{campaign-end}}!' +
			'We will be issuing a check in support after {{campaign-end}}. Best of luck! \n' + this.userInfo.firstName;

		this.userInfo.address = this.organization.address;
		this.userInfo.city = this.organization.city;
		this.userInfo.state = this.organization.state;
		this.userInfo.recEmail = true;
		this.userInfo.postalCode = this.organization.zip;

		this.route.queryParams.subscribe(params => {
			this.subscriptionParam = params['subscription'];
		});

		if (this.isLoggedIn()) {
			const curUser: CustomUser = this.auth.getCurrentUserData();
			this.userApi.getCurrent().flatMap((token: SDKToken) => {
				return this.orgApi.create(this.organization).map(o => [o, token]);
			}).subscribe(([organization, token]: [Organization, SDKToken]) => {
				console.log('Current User: ', curUser);

				console.log('Trying to pass this org: ', organization.id);

				this.flashMessageService.showMessage({
					message: 'Additional organization created successfully',
					messageClass: 'success'
				});

				if (curUser.organizations) {
					curUser.organizations.push(organization);
				} else {
					curUser.organizations = [organization];
				}
				curUser.orgId = organization.id;

				//set the current organization
				this.curOrgSrv.setCurrentItemObj(organization);

				this.auth.setUser(curUser);
				this.auth.save();

				console.log('Current User Orgs: ', curUser.organizations);

				this.router.navigateByUrl('/configure/' + organization.id);
			}, err => {
				const details = err.details || {messages: {a: 'Unknown error. Please try again Later.'}};
				const context = details.context || '';
				console.log('got error saving org', err);
				this.flashMessageService.showError(
					(context === 'CustomUser' ? 'User' : 'Organization') + ' error:' +
					Object.keys(details.messages).map(k => details.messages[k]).join(', ')
				);
			});
		} else {
			this.userApi.create(this.userInfo)
				.flatMap(user => {
					return this.userApi.login({email: this.userInfo.email, password: this.userInfo.password});
				}/*, err => {
					//TODO this should probably actually check the error that comes back
					this.flashMessageService.showMessage({message: 'Email already in use', messageClass: 'danger'});
					throw err;
				}*/
				)
				.flatMap((token: SDKToken) => {
					return this.orgApi.create(this.organization).map(o => [o, token]);
				}).subscribe(([organization, token]: [Organization, SDKToken]) => {
					console.log('trying to pass this org: ', this.userInfo.orgId);
					this.flashMessageService.showMessage({
						message: 'Account created successfully',
						messageClass: 'success'
					});


					console.log('user logged in');
					token.user.organizations = [organization];
					token.user.orgId = organization.id;

					this.auth.setUser(token.user);
					this.auth.save();

					if (this.subscriptionParam && this.subscriptionParam === 'risk-free') {
						console.log('Risk-free');
						const giveshareSite = 'giveshare';
						const riskFreeLink = `https://${giveshareSite}.chargebee.com/hosted_pages/plans/standard-monthly-subscription?customer[cf_giveshare_customer_id]=${organization.id}&subscription[coupon]=OneDollarMonth`;
						window.open(riskFreeLink, '_blank');
					}

					this.router.navigateByUrl('/configure/' + organization.id);
				}, err => {
					const details = err.details || {messages: {a: 'Unknown error. Please try again Later.'}};
					const context = details.context || '';
					console.log('got error saving org', err);
					this.flashMessageService.showError(
						(context === 'CustomUser' ? 'User' : 'Organization') + ' error:' +
						Object.keys(details.messages).map(k => details.messages[k]).join(', ')
					);
				});
		}
	}

	async showPicker() {
		const client = filestack.init('AOkwl2T1TwGjNpJEXRC3wz');
		const result = await client.pick({ maxFiles: 1 });
		console.log('res', result);
		console.log('file', result.filesUploaded[0]);
		const url = result.filesUploaded[0].url;
		this.organization.logo = url;
	}
}
