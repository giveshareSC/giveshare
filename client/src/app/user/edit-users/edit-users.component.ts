import { Component, OnInit } from '@angular/core';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CustomUserApi} from '../../shared/sdk/services/custom';
import {OrganizationUserApi} from '../../shared/sdk/services/custom/OrganizationUser';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
	selector: 'app-edit-users',
	templateUrl: './edit-users.component.html',
	styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {

	userInfo = new CustomUser();
	userList: CustomUser[];
	displayError: string = null;
	isEditing = false;
	isNew = false;

	private organizationId: string;

	constructor(private userApi: CustomUserApi, private router: Router, private orgUserApi: OrganizationUserApi, private route: ActivatedRoute,
				private flashMessageService: FlashMessageService, private orgApi: OrganizationApi, private auth: LoopBackAuth) { }

	ngOnInit() {
		//TODO pull org id from user
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) return;


		this.route.params
			.map(params => params['orgId'])
			.filter(orgId => orgId)
			.flatMap(orgId => {
				this.organizationId = orgId;
				console.log('looking for org for user edits', this.organizationId);
				return this.orgApi.getCustomUsers(orgId);
			}).subscribe((users: CustomUser[]) => {
				console.log('entered edit init code', this.organizationId);
				this.userList = users;
			});
	}

	createUser() {
		this.userInfo.orgId = this.organizationId.toString();
		console.log('trying to pass this org: ', this.userInfo.orgId);
		this.userApi.create(this.userInfo).subscribe(user => {

			this.orgUserApi.create({organizationId: this.organizationId, userId: user.id}).subscribe(r => {
				console.log('orgUserCreated');
			});

			this.flashMessageService.showMessage({message: 'User added successfully', messageClass: 'success'});
			this.router.navigateByUrl('/user-edit/' + this.organizationId);
			this.isEditing = false;
			this.isNew = false;
			this.ngOnInit();
		}, err => {
			//TODO this should probably actually check the error that comes back
			this.flashMessageService.showMessage({message: 'Email already in use', messageClass: 'danger'});
			console.log('err somewhere', err);
		});
	};

	updateUser(userInfo) {
		userInfo.orgId = this.organizationId;
		console.log('trying to update this user: ', userInfo);
		this.userApi.patchAttributes(userInfo.id, userInfo).subscribe(user => {

			this.flashMessageService.showMessage({message: 'User updated successfully', messageClass: 'success'});
			this.router.navigateByUrl('/user-edit/' + this.organizationId);
			this.isEditing = false;
			this.isNew = false;
		}, err => {
			//TODO this should probably actually check the error that comes back
			this.flashMessageService.showMessage({message: 'failed to update user', messageClass: 'danger'});
			console.log('err somewhere', err);
		});
	};

	editUser(user: CustomUser) {
		console.log('editing user', user);
		this.userInfo = user;
		this.isEditing = true;
		this.isNew = false;
	}

	newUser() {
		console.log('new user called');
		this.userInfo = new CustomUser();
		this.isEditing = false;
		this.isNew = true;
	}

	deleteUser(user: CustomUser) {
		if (confirm(`Are you sure you want to delete ${user.firstName} ${user.lastName} (${user.id})`)) {
			this.userApi.deleteById(user.id).subscribe(() => {
				this.displayError = null;
				this.userList.splice(this.userList.findIndex(u => u.id === user.id), 1);
				this.flashMessageService.showMessage({message: 'Deleted User', messageClass: 'success'});
			}, err => {
				this.flashMessageService.showMessage({message: this.getErrorMessage(err), messageClass: 'danger'});
			});
			this.router.navigateByUrl('/user-edit/' + this.organizationId);
			this.isEditing = false;
		}
	}

	cancelEdit() {
		this.isEditing = false;
		this.isNew = false;
	}

	private getErrorMessage (err: any) {
		return (err instanceof Error || typeof err === 'object' && err.message) ? err.message : err.toString();
	}
}

