import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {LoginModel} from './login.model';
import {CustomUserApi, LoopBackAuth} from '../../shared/sdk/services';
import {SDKToken} from '../../shared/sdk/models';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {LoginPageServiceService} from '../../login-page-service/login-page-service.service';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import {CurrentOrganizationService} from '../../shared/current-item.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	loginInfo = new LoginModel();

	constructor(private userApi: CustomUserApi, private auth: LoopBackAuth, private router: Router,
				private flashMessageService: FlashMessageService, private pageService: LoginPageServiceService,
				private curOrgSrv: CurrentOrganizationService
	) {
		this.loginInfo.rememberMe = true;
	}

	ngOnInit() {
	}

	login() {
		this.userApi.login(this.loginInfo, 'user', this.loginInfo.rememberMe).flatMap((token: SDKToken) => {
			//once the user has been logged in, look up their roles and save them to the user
			return this.userApi.findById(token.user.id, {include: ['organizationUsers', 'organizations', 'roles']}).map(u => [u, token]);
		}).subscribe(([user, token]: [CustomUser, SDKToken]) => {
			token.user.roles = user.roles;
			token.user.organizations = user.organizations;
			token.user.organizationUsers = user.organizationUsers;
			token.user.testField = 'This is a test';

			//if only one organization, then pre-select that
			if (token.user.organizations && token.user.organizations.length === 1) {
				token.user.orgId = token.user.organizations[0].id;
			}

			this.auth.setUser(token.user);
			this.auth.save();

			this.flashMessageService.showMessage({message: 'Logged in successfully', messageClass: 'success'});
			this.router.navigateByUrl(this.pageService.getPage() || '/home/' + token.user.orgId).then(r => {
				//mark that there is a new org. This should reload page links, etc.
				this.curOrgSrv.setCurrentItemObj(token.user.organizations[0]);
			});

		}, err => {
			this.flashMessageService.showMessage({message: 'Invalid Login', messageClass: 'danger'});
		});
	}
}
