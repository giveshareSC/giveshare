import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, CollapseModule, ButtonsModule} from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {WebsiteComponent} from './Website/website.component';
import { LoggedInGuard } from './guards/login';
import { SuperuserGuard } from './guards/superuser';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';
import { CreateAccountComponent } from './user/create-account/create-account.component';
import { ProfileComponent } from './user/profile/profile.component';
import { SDKBrowserModule } from './shared/sdk';
import { AdminComponent } from './admin/admin.component';
import { UserListComponent } from './admin/user-list/user-list.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
//import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { FlashMessageComponent } from './flash-message/flash-message.component';
import { FlashMessageService } from './flash-message/flash-message.service';
import { SocketService } from './shared/socket.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CampaignCreateComponent } from './organization/campaign-create/campaign-create.component';
import { ResponseConfigureComponent } from './organization/response-configure/response-configure.component';
import {
	CampaignResponseComponent,
	CompletedCampaignResponseComponent
} from './organization/campaign-response/campaign-response.component';
import { OrgConfigureComponent } from './organization/org-configure/config.landingpage.component';
import { OrganizationDashboardComponent } from './organization/organization-dashboard/organization-dashboard.component';
import {HomePageComponent} from './organization/HomePage/HomePage.component';
import {EditUsersComponent} from './user/edit-users/edit-users.component';
import {LoopBackConfig} from './shared/sdk';
import {DbComponent} from './organization/db/db.component';
import {OrgSetupComponent} from './organization/Organization-Setup/organization-setup.component';
import {CampaignReportingComponent} from './organization/Reporting/reporting.component';
import { CampaignRequestComponent } from './organization/campaign-request/campaign-request.component';
import {CampaignRedemptionComponent} from './organization/campaign-redemption/campaign-redemption.component';
import {CustomizedComponent} from './organization/Customized/customized.component';
import {ClosedReportComponent} from './organization/closed-report/closed-report.component';
import { PageTitleService } from './page-title/page-title.service';
import {HeaderService} from './header/header.service';
import {ExpiredCampaignComponent} from './organization/expired-campaigns/expired-campaigns.component';
import {LoginPageServiceService} from './login-page-service/login-page-service.service';
import { ContactComponent } from './contact/contact.component';
import {SubscriptionComponent} from './organization/subscription/subscription.component';
import { ValueCalculatorComponent } from './value-calculator/value-calculator.component';
import {SubscriptionSelectionService} from './organization/subscription/selection/subscription-selection.service';
import { OfferOptionsComponent } from './organization/settings/offer-options/offer-options.component';
import { CampaignDaysComponent } from './organization/settings/campaign-days/campaign-days.component';
import { NotificationScheduleComponent } from './organization/settings/notification-schedule/notification-schedule.component';
import { ResponseConfigurationComponent } from './organization/settings/response-configuration/response-configuration.component';
import { CouponTimesComponent } from './organization/settings/coupon-times/coupon-times.component';
import { DefaultEmailComponent } from './organization/settings/default-email/default-email.component';
import { TermsConditionsComponent } from './organization/settings/terms-conditions/terms-conditions.component';
import { SocialMediaComponent } from './organization/settings/social-media/social-media.component';
import { DefaultEmailSupporterComponent } from './organization/settings/default-email-supporter/default-email-supporter.component';
import { TermsComponent } from './terms/terms.component';
import {CurrentOrganizationService} from './shared/current-item.service';
import {SetupService} from './organization/settings/setup.service';
import { environment } from '../environments/environment';

// LoopBackConfig.setBaseURL('//localhost:3000');
LoopBackConfig.setBaseURL(environment.apiRoot);

@NgModule({
	declarations: [
		AppComponent,
		WebsiteComponent,
		LoginComponent,
		CreateAccountComponent,
		ProfileComponent,
		AdminComponent,
		UserListComponent,
		AdminDashboardComponent,
		//ResetPasswordComponent,
		FlashMessageComponent,
		DashboardComponent,
		CampaignCreateComponent,
		ResponseConfigureComponent,
		CampaignReportingComponent,
		CampaignRedemptionComponent,
		OrgConfigureComponent,
		OrgSetupComponent,
		OrganizationDashboardComponent,
		HomePageComponent,
		EditUsersComponent,
		ClosedReportComponent,
		DbComponent,
		CampaignResponseComponent,
		CompletedCampaignResponseComponent,
		CustomizedComponent,
		ExpiredCampaignComponent,
		CampaignRequestComponent,
		ContactComponent,
		SubscriptionComponent,
		ValueCalculatorComponent,
		OfferOptionsComponent,
		CampaignDaysComponent,
		NotificationScheduleComponent,
		ResponseConfigurationComponent,
		CouponTimesComponent,
		DefaultEmailComponent,
		TermsConditionsComponent,
		SocialMediaComponent,
		DefaultEmailSupporterComponent,
		TermsComponent
	],
	imports: [
		BrowserModule,
		BsDropdownModule.forRoot(),
		CollapseModule.forRoot(),
		TooltipModule.forRoot(),
		ButtonsModule.forRoot(),
		FormsModule,
		HttpModule,
		routing,
		RouterModule,
		SDKBrowserModule.forRoot()
	],
	providers: [
		LoggedInGuard,
		SuperuserGuard,
		FlashMessageService,
		SocketService,
		HeaderService,
		PageTitleService,
		LoginPageServiceService,
		SubscriptionSelectionService,
		CurrentOrganizationService,
		SetupService,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
