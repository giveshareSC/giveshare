import { Component, OnInit } from '@angular/core';
// import {CampaignInterface, OrganizationInterface} from '../campaign';
//import {CampaignApi, CampaignInterface, OrganizationInterface, Organization, OrganizationApi} from '../../shared/sdk/';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../flash-message/flash-message.service';
import {HeaderService} from '../header/header.service';
import {SubscriptionSelectionService} from '../organization/subscription/selection/subscription-selection.service';

@Component({
	selector: 'app-website-component',
	templateUrl: './website.component.html',
	styleUrls: ['./website.component.css']
})
export class WebsiteComponent implements OnInit {

	showPics = true;
	showLogos = false;
	showPricing = true;

	constructor(private route: ActivatedRoute, private router: Router, private flashMessage: FlashMessageService,
				headerService: HeaderService, private subscriptionSelection: SubscriptionSelectionService) {
		headerService.setHeader(false);
	}

	ngOnInit() {
	}

	scrollToHash(hash) {
		const target = document.getElementById(hash);

		target.scrollIntoView({behavior: 'smooth'});
	}

	setSelection(selection: string) {
		this.subscriptionSelection.setSelection(selection);
	}
}
