import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';

@Injectable()
export class FlashMessageService {

	private messagesToShowSource = new Subject<FlashMessageInterface>();

	// Observable string streams
	messagesToShow$ = this.messagesToShowSource.asObservable();

	constructor() { }

	// Service message commands
	showMessage(message: FlashMessageInterface) {
		this.messagesToShowSource.next(message);
	}

	showSuccess(message: string) {
		this.showMessage({message, messageClass: 'success'});
	}

	showError(message: string) {
		this.showMessage({message, messageClass: 'danger'});
	}

}
export interface FlashMessageInterface {
	message: string;
	messageClass: string;
}
