import {Component, OnInit} from '@angular/core';
import {GeneralApi} from '../shared/sdk/services/custom/General';
import {FlashMessageService} from '../flash-message/flash-message.service';
import {Router} from '@angular/router';

class ContactModel {
	contactName: string;
	contactEmail: string;
	contactPhone: string;
	message: string;
	captchaResponse: string;
}

@Component({
	selector: 'app-contact',
	templateUrl: './contact.component.html',
	styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

	info: ContactModel = new ContactModel();

	constructor(private generalApi: GeneralApi, private flashMessage: FlashMessageService, private router: Router) {
	}

	ngOnInit() {
		this.info.captchaResponse = 'sdfsdf';
	}

	submitRequest() {
		if (!this.info.contactName || !this.info.contactEmail || !this.info.message) {
			this.flashMessage.showError('Please complete required fields');
			return;
		}

		if (!this.info.captchaResponse) {
			this.flashMessage.showError('Please complete captcha');
			return;
		}

		this.generalApi.addContactRequest(
			this.info.contactName,
			this.info.contactEmail,
			this.info.contactPhone,
			this.info.message,
			this.info.captchaResponse
		).subscribe(res => {
			if (res.success) {
				this.flashMessage.showSuccess('Message sent');
				this.router.navigateByUrl('/');
			} else {
				this.flashMessage.showError(res.message);
			}
		});
	}
}
