import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class HeaderService {

	// private title = 'Give Share';

	private header$ = new Subject<boolean>();

	constructor() {
		this.header$.next(true);
	}

	setHeader(newHeader) {
		console.log('set header', newHeader);
		// this.title = newTitle;
		this.header$.next(newHeader);
	}

	getHeader() {
		console.log('set header');
		return this.header$.asObservable();
	}

}
