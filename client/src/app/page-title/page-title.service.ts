import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class PageTitleService {

	// private title = 'Give Share';

	private title$ = new Subject<string>();

	constructor() {
		this.title$.next('Give Share');
	}

	setTitle(newTitle) {
		console.log('new title', newTitle);
		// this.title = newTitle;
		this.title$.next(newTitle);
	}

	getTitle() {
		console.log('get title');
		return this.title$.asObservable();
	}

}
