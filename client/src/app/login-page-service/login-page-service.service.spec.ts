import { TestBed, inject } from '@angular/core/testing';

import { LoginPageServiceService } from './login-page-service.service';

describe('LoginPageServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginPageServiceService]
    });
  });

  it('should be created', inject([LoginPageServiceService], (service: LoginPageServiceService) => {
    expect(service).toBeTruthy();
  }));
});
