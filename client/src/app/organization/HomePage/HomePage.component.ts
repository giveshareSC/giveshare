import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {PageTitleService} from '../../page-title/page-title.service';
import {HeaderService} from '../../header/header.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

declare const filestack: {
	init(apiKey: string): {
		pick({ maxFiles }: { maxFiles: number }):
			Promise<{ filesUploaded: { url: string }[] }>
	}
};

@Component({
	selector: 'app-homepage',
	templateUrl: './homepage.sc.component.html',
	styleUrls: ['./HomePage.component.css']
})
export class HomePageComponent implements OnInit {

	private organizationId: string;
	private campaignId: string;

	organization = new Organization();
	campaign = new Campaign();
	report = false;
	dashboard = true;
	campaigns: Campaign[];
	openCampaigns: Campaign[];
	unclosedCampaigns: Campaign[];
	expired = 0;
	active = 0;
	requested = 0;
	closed = 0;
	dbData = {
		action: 0,
		review: 0,
		completed: 0
	};

	summary = {};

	crap = Math.random() * 100;

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute, private campApi: CampaignApi,
				private flashMessageService: FlashMessageService, private auth: LoopBackAuth, titleService: PageTitleService,
				headerService: HeaderService) {
		titleService.setTitle('Welcome back to GiveShare!');
		headerService.setHeader(true);
	}

	ngOnInit() {
		console.log('report set?', this.report);
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) {
			this.flashMessageService.showMessage({messageClass: 'danger', message: 'You are not logged in'});
			return;
		}

		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.map(params => params['orgId'])
			.filter(orgId => orgId)
			.flatMap(orgId => {
				this.organizationId = orgId;
				console.log('looking for org', orgId);
				return Observable.forkJoin(
					this.orgApi.getCampaigns(orgId),
					this.orgApi.organizationSummary(orgId)
				);
			}).subscribe(([camps, summ]: [Campaign[], any]) => {
				this.campaigns = camps;
				this.summary = summ;
				this.unclosedCampaigns = camps.filter(c => c.status !== 'closed');
				this.openCampaigns = camps.filter(c => c.status === 'active');

				//if any of the campaigns are missing a discount percentage, default one
				this.campaigns.filter(c => typeof c.discountPercentage === 'undefined')
					.forEach(c => c.discountPercentage = 0);
				console.log('entered init code', this.campaigns);

				if (this.dashboard) {
					this.getDbData();
				}
			});

	}

	getDbData () {
		//check for expired
		this.expired = this.campaigns.filter(c => c.status === 'expired').length;
		//check for requested
		this.requested = this.campaigns.filter(c => c.status === 'requested').length;

		//check for closed
		this.dbData.completed = this.campaigns.filter(c => c.status === 'closed').length;
		//check for active
		this.dbData.review = this.campaigns.filter(c => c.status === 'active').length;


		this.dbData.action = this.expired + this.requested;
	}

	reqAction() {
		console.log('how many requested', this.requested);
		if (this.requested !== 0) {
			this.router.navigateByUrl('/Customized/' + this.organizationId);
		} else {
			this.router.navigateByUrl('/Expired/' + this.organizationId);
		}
	}

	gotoReports() {
		this.router.navigateByUrl('/Reporting/' + this.organizationId);
	}
	goBack() {
		this.dashboard = true;
		this.report = false;
		this.router.navigateByUrl('/home/' + this.organizationId);
	}

	openCamps() {
		this.dashboard = false;
		this.report = true;
		this.router.navigateByUrl('/home/' + this.organizationId);
	}

	payAndClose(campaign: Campaign) {
		this.campaign = campaign;
		this.campaign.status = 'closed';
		this.campaignId = this.campaign.id;
		console.log('campaing to close:', this.campaignId);
		this.campApi.patchAttributes(this.campaign.id, this.campaign).subscribe(result => {
			this.flashMessageService.showMessage({
				message: 'Campaign has been closed!!',
				messageClass: 'success'
			});
			this.router.navigateByUrl('/home/' + this.organizationId);
		});
		this.router.navigateByUrl('/home/' + this.organizationId);
	}

}
