import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import { DatePipe } from '@angular/common';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {PageTitleService} from '../../page-title/page-title.service';

@Component({
	selector: 'app-campaign-expired',
	templateUrl: './expired-campaigns.component.html',
	styleUrls: ['./expired-campaigns.component.css'],
	providers: [DatePipe]
})

export class ExpiredCampaignComponent implements OnInit {

	campaigns: Campaign[];
	dateYesterday = new Date();
	dateToday = new Date();

	constructor(private router: Router, private campApi: CampaignApi,
				private datePipe: DatePipe, titleService: PageTitleService,
				private flashMessageService: FlashMessageService, private route: ActivatedRoute, private auth: LoopBackAuth) {
		titleService.setTitle('Expired Campaigns');
	}

	ngOnInit() {

		this.dateYesterday = new Date(new Date().getTime() - (24 * 60 * 60 * 1000));
		console.log('date returned:', this.dateToday, this.dateYesterday);
		const filter = {
			where: {
				between: [this.dateToday, this.dateYesterday]
			},
		};
		// this.campApi.find(filter).subscribe(camp => this.campaigns = camp);
	}

}
