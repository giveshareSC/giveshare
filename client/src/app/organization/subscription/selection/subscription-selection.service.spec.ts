import { TestBed, inject } from '@angular/core/testing';

import { SubscriptionSelectionService } from './subscription-selection.service';

describe('SubscriptionSelectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscriptionSelectionService]
    });
  });

  it('should be created', inject([SubscriptionSelectionService], (service: SubscriptionSelectionService) => {
    expect(service).toBeTruthy();
  }));
});
