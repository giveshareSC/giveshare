import {Injectable} from '@angular/core';

@Injectable()
export class SubscriptionSelectionService {

	selection = '';

	constructor() {
		//kinda useful track the status of a service in the console
		// (window as any).selectionThing = this;
	}

	setSelection(selection: string) {
		this.selection = selection;
	}

	getSelection() {
		return this.selection;
	}

}
