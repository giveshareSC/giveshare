import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models';
import {OrganizationApi} from '../../shared/sdk/services/custom';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {PageTitleService} from '../../page-title/page-title.service';
import {HeaderService} from '../../header/header.service';
import {SubscriptionSelectionService} from './selection/subscription-selection.service';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
	selector: 'app-organization-subscription',
	templateUrl: './subscription.component.html',
	styleUrls: ['./subscription.component.less']
})
export class SubscriptionComponent implements OnInit {
	organization = new Organization();

	readonly giveshareSite = 'giveshare';
	readonly portalLink = `https://${this.giveshareSite}.chargebeeportal.com/`;
	readonly subscribeRoot = `https://${this.giveshareSite}.chargebee.com/hosted_pages/plans/`;
	subscribeLinkAnnual = '';
	subscribeLinkMonthly = '';

	isSubValid = false;

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute,
				private flashMessageService: FlashMessageService, titleService: PageTitleService, header: HeaderService,
				public subscriptionSelection: SubscriptionSelectionService) {
		titleService.setTitle('Subscription');
		header.setHeader(true);
	}

	ngOnInit() {
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.filter(params => params['orgId'])
			.map(params => params['orgId'])
			.flatMap(orgId => this.orgApi.findById(orgId))
			.subscribe((org: Organization) => {
				this.organization = org;

				if (typeof this.organization.subscriptionRenewalDate === 'string') {
					this.organization.subscriptionRenewalDate = new Date(this.organization.subscriptionRenewalDate);
				}
				const urlParams = `?customer[cf_giveshare_customer_id]=${this.organization.id}`;
				this.subscribeLinkAnnual = `${this.subscribeRoot}standard-annual-subscription${urlParams}`;
				this.subscribeLinkMonthly = `${this.subscribeRoot}standard-monthly-subscription${urlParams}`;
				this.isSubValid = this.organization.subscriptionRenewalDate && this.organization.subscriptionRenewalDate.getTime() > Date.now();
			});
	}
}
