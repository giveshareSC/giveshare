import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {CampaignDetails} from '../../shared/sdk/models/CampaignDetails';
import {CampaignDetailsApi} from '../../shared/sdk/services/custom/CampaignDetails';
import {PageTitleService} from '../../page-title/page-title.service';

@Component({
	selector: 'app-closed-report',
	templateUrl: './closed-report.component.html',
	styleUrls: ['./closed-report.component.css']
})
export class ClosedReportComponent implements OnInit {

	private organizationId: string;
	private CampaignId: string;

	organization = new Organization();
	newSales = 0;
	oldSales = 0;
	campaign = new Campaign();
	campaigns: Campaign[];
	details: CampaignDetails[];
	recDetails = new CampaignDetails();
	Details = false;
	noDetails = true;
	editrec = false;

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute, private campApi: CampaignApi,
				private campdetApi: CampaignDetailsApi, private flashMessageService: FlashMessageService, private auth: LoopBackAuth,
				titleService: PageTitleService) {
		titleService.setTitle('Expired Campaigns Report');
	}

	ngOnInit() {
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) {
			this.flashMessageService.showMessage({messageClass: 'danger', message: 'You are not logged in'});
			return;
		}

		this.organizationId = curUser.orgId;

		const filter = {
			where: {
				status: 'expired'
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.organizationId = params['orgId'];
				console.log('looking for org', this.organizationId, filter);
				this.orgApi.getCampaigns(this.organizationId, filter).subscribe(camps => {
					this.campaigns = camps;
					});
					//if any of the campaigns are missing a discount percentage, default one
					this.campaigns.filter(c => typeof c.discountPercentage === 'undefined')
						.forEach(c => c.discountPercentage = 0);
					console.log('entered init code', this.campaigns);

			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});
	}

	payAndClose(campaign: Campaign) {
		this.noDetails = true;
		this.Details = false;
		this.campaign = campaign;
		this.campaign.status = 'closed';
		this.CampaignId = this.campaign.id;
		console.log('campaign to close:', this.CampaignId);
		this.campApi.patchAttributes(this.campaign.id, this.campaign).subscribe(result => {
			this.flashMessageService.showMessage({
				message: 'Campaign has been closed!!',
				messageClass: 'success'
			});
			this.router.navigateByUrl('/ClosedReport/' + this.organizationId);
		});
	}
	detailScreen(campaign: Campaign) {
		this.Details = true;
		this.noDetails = false;
		this.campaign = campaign;
		console.log('looking up details', this.campaign.id);
		this.campApi.getDetails(this.campaign.id).subscribe(deets => this.details = deets);
		this.router.navigateByUrl('/ClosedReport/' + this.organization.id);
	}
	goBack() {
		this.Details = false;
		this.noDetails = true;
		this.ngOnInit();
	}

	goBacktoDets() {
		this.Details = true;
		this.noDetails = false;
		this.editrec = false;
		this.ngOnInit();
	}

	editRecord(detail: CampaignDetails) {
		console.log('editing coupon', detail);
		console.log('is this better?', this.details);
		this.recDetails = detail;
		this.oldSales = this.recDetails.TotalSales;
		this.editrec = true;
		this.Details = false;
		this.noDetails = false;
	}

	updateCoupon() {

		console.log('looking to update coupon', this.recDetails.id);
		this.campdetApi.patchAttributes(this.recDetails.id, this.recDetails).subscribe(result => {

			this.flashMessageService.showMessage({
				message: 'You have updated the record!!',
				messageClass: 'success'
			});
			this.editrec = false;
			this.Details = true;
			this.router.navigateByUrl('/ClosedReport/' + this.organizationId);
			this.CampaignId = this.recDetails.CampaignId;
			this.campApi.findById(this.CampaignId).subscribe((camps: Campaign) => {
				this.campaign = camps;
				console.log('current camp amt', this.campaign.resultingAmount);
				console.log('details $$ adjusted:', this.recDetails.TotalSales);
				console.log('trying to subtract:', this.oldSales);
				this.newSales = (this.campaign.resultingAmount - this.oldSales);
				this.newSales = (this.newSales + this.recDetails.TotalSales);
				console.log('newSales var:', this.newSales);
				this.campaign.resultingAmount = this.newSales;

				this.campApi.patchAttributes(this.CampaignId, this.campaign).subscribe(res => {
					console.log('updated amount:', this.campaign.id, this.campaign.resultingAmount);
				}, err => {
					console.log('cant update campaign', this.CampaignId, err);
				});
			}, err => {
				console.log('cant find campaign', this.CampaignId);
			});
		});
	}

}
