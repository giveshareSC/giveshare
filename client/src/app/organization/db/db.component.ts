import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {PageTitleService} from '../../page-title/page-title.service';
import {HeaderService} from '../../header/header.service';

declare const filestack: {
	init(apiKey: string): {
		pick({ maxFiles }: { maxFiles: number }):
			Promise<{ filesUploaded: { url: string }[] }>
	}
};

@Component({
	selector: 'app-db',
	templateUrl: './db.component.html',
	styleUrls: ['./db.component.css']
})
export class DbComponent implements OnInit {

	private organizationId: string;
	// private campaignId: string;

	organization = new Organization();
	campaign = new Campaign();
	campaigns: Campaign[];

	summary = {};
	expired = 0;
	active = 0;
	requested = 0;
	closed = 0;
	dbData = {
		action: 0,
		review: 0,
		completed: 0
	};

	crap = Math.random() * 100;

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute, private campApi: CampaignApi,
				private flashMessageService: FlashMessageService, private auth: LoopBackAuth, titleService: PageTitleService,
				headerService: HeaderService) {
		titleService.setTitle('GiveShare Dashboard');
		headerService.setHeader(true);
	}

	ngOnInit() {
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) {
			this.flashMessageService.showMessage({messageClass: 'danger', message: 'You are not logged in'});
			return;
		}
		this.organizationId = curUser.orgId;

//check for expired
		const filter = {
			where: {
				status: 'expired'
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.organizationId = params['orgId'];

				console.log('looking for org', this.organizationId, filter);
				this.orgApi.getCampaigns(this.organizationId, filter).subscribe((camps: Campaign[]) => {
					this.campaigns = camps;
					this.expired = this.campaigns.length;
					this.dbData.action = this.expired;
					console.log('number of expired', this.expired);

				});
			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});

		//check for requested

		const filter2 = {
			where: {
				status: 'requested'
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {

				this.orgApi.getCampaigns(this.organizationId, filter2).subscribe((camps: Campaign[]) => {
					this.campaigns = camps;
					this.requested = this.campaigns.length;
					this.dbData.action = (this.requested + this.dbData.action);
					console.log('number of requested', this.requested);

				});
			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});

		console.log('number of actions', this.dbData.action);

//check for closed

		const filter3 = {
			where: {
				status: 'closed'
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {

				this.orgApi.getCampaigns(this.organizationId, filter3).subscribe((camps: Campaign[]) => {
					this.campaigns = camps;
					this.dbData.completed = this.campaigns.length;
					console.log('number of closed', this.dbData.completed);

				});
			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});

		//check for active

		const filter4 = {
			where: {
				status: 'active'
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {

				this.orgApi.getCampaigns(this.organizationId, filter4).subscribe((camps: Campaign[]) => {
					this.campaigns = camps;
					this.dbData.review = this.campaigns.length;
					console.log('number of active', this.dbData.review);

				});
			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});
	}

	reqAction() {
		this.router.navigateByUrl('/Customized/' + this.organizationId);
	}

}
