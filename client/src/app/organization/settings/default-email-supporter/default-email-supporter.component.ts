import { Component, OnInit } from '@angular/core';
import {Organization} from '../../../shared/sdk/models';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SettingPageBase} from '../setting-page-base';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';


@Component({
	selector: 'app-default-email-supporter',
	templateUrl: './default-email-supporter.component.html',
	styleUrls: ['./default-email-supporter.component.css']
})
export class DefaultEmailSupporterComponent extends SettingPageBase {
	compTitle = 'Configure requester response';
	editinvite = false;

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}

	editInvite() {
		this.editinvite = true;
		console.log('specific hours?', this.organization.specifyHours);
	}

	saveInvite() {
		this.editinvite = false;
		this.organization.setupComplete = true;
		this.save();
	}

}
