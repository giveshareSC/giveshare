import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultEmailSupporterComponent } from './default-email-supporter.component';

describe('DefaultEmailSupporterComponent', () => {
  let component: DefaultEmailSupporterComponent;
  let fixture: ComponentFixture<DefaultEmailSupporterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultEmailSupporterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultEmailSupporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
