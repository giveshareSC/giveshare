import { Component, OnInit } from '@angular/core';
import {Organization} from '../../../shared/sdk/models';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {SettingPageBase} from '../setting-page-base';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
	selector: 'app-notification-schedule',
	templateUrl: './notification-schedule.component.html',
	styleUrls: ['./notification-schedule.component.css']
})
export class NotificationScheduleComponent extends SettingPageBase {
	compTitle = 'Notification Schedule';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}
}
