import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDaysComponent } from './campaign-days.component';

describe('CampaignDaysComponent', () => {
  let component: CampaignDaysComponent;
  let fixture: ComponentFixture<CampaignDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
