import { Component} from '@angular/core';
import {SettingPageBase} from '../setting-page-base';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
	selector: 'app-campaign-days',
	templateUrl: './campaign-days.component.html',
	styleUrls: ['./campaign-days.component.css']
})
export class CampaignDaysComponent extends SettingPageBase {
	compTitle = 'Notifications (New Campaigns)';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}
}
