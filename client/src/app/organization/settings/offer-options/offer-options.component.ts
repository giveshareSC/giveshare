import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Organization} from '../../../shared/sdk/models';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {SettingPageBase} from '../setting-page-base';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';

@Component({
	selector: 'app-offer-options',
	templateUrl: './offer-options.component.html',
	styleUrls: ['./offer-options.component.css']
})
export class OfferOptionsComponent extends SettingPageBase {
	compTitle = 'Configure Offer Options';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}
}
