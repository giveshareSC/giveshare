import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CouponTimesComponent } from './coupon-times.component';

describe('CouponTimesComponent', () => {
  let component: CouponTimesComponent;
  let fixture: ComponentFixture<CouponTimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CouponTimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CouponTimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
