import { Component, OnInit } from '@angular/core';
import {Organization} from '../../../shared/sdk/models';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';
import {SettingPageBase} from '../setting-page-base';

@Component({
	selector: 'app-coupon-times',
	templateUrl: './coupon-times.component.html',
	styleUrls: ['./coupon-times.component.css']
})
export class CouponTimesComponent extends SettingPageBase {
	compTitle = 'Configure Component Times';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}

	validate() {
		const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
		const isValid = Array.prototype.concat.apply(
			[],
			days.map(d => [`${d}1`, `${d}2`])
		).some(d => this.organization[`${d}FromTime`] && this.organization[`${d}ToTime`]);

		if (isValid) {
			return isValid;
		} else {
			this.flashMessageService.showError('Please supply at least one set of start and stop times.');
		}
	}

}
