import { Component} from '@angular/core';
import {SettingPageBase} from '../setting-page-base';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
	selector: 'app-terms-conditions',
	templateUrl: './terms-conditions.component.html',
	styleUrls: ['./terms-conditions.component.css']
})
export class TermsConditionsComponent extends SettingPageBase {
	compTitle = 'Configure Terms and Conditions';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}
}
