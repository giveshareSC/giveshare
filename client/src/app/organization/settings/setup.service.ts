import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {Organization} from '../../shared/sdk/models';
import {CurrentOrganizationService} from '../../shared/current-item.service';

export class SettingPage {
	name: string;
	path: string;
	enabled: boolean;

	constructor(name: string, path: string, enabled = true) {
		this.name = name;
		this.path = path;
		this.enabled = enabled;
	}
}

@Injectable()
export class SetupService {
	private currentItemSource = new Subject<boolean>();
	private dirtyStatus = false;

	private cachedOrg: Organization;

	private pageList: SettingPage[] = [
		new SettingPage('Set Offer Options', 'offer-options'),
		new SettingPage('Notifications (New Campaigns)', 'campaign-days'),
		new SettingPage('Notifications (Closed Campaigns)', 'notification-schedule'),
		new SettingPage('Campaign Defaults', 'response-configuration'),
		new SettingPage('Coupon Valid Times', 'coupon-times'),
		new SettingPage('Default Email (Charity)', 'default-email'),
		new SettingPage('Terms and Conditions', 'terms-conditions'),
		new SettingPage('Content for Social Media', 'social-media'),
		new SettingPage('Default Email (Supporter)', 'default-email-supporter'),
	];

	constructor(curOrgSvr: CurrentOrganizationService) {
		console.log('subscribing');
		curOrgSvr.getCurrentItem().subscribe(newOrg => {
			if (this.cachedOrg && this.cachedOrg.id) {
				if (this.cachedOrg.specifyHours !== newOrg.specifyHours) {
					console.log('checking hours 3', this.cachedOrg.specifyHours, newOrg.specifyHours);
					const timeConf = this.pageList.find(p => p.path === 'coupon-times');
					timeConf.enabled = newOrg.specifyHours;
				} else if (this.cachedOrg.allowCustom !== newOrg.allowCustom) {
					console.log('checking custom 3', this.cachedOrg.allowCustom, newOrg.allowCustom);
					const customizeConf = this.pageList.find(p => p.path === 'campaign-days');
					customizeConf.enabled = newOrg.allowCustom;
				}
			} else {
				console.log('found new org. resetting');
				//this is the first load of the real org
				const customizeConf = this.pageList.find(p => p.path === 'campaign-days');
				const timeConf = this.pageList.find(p => p.path === 'coupon-times');
				customizeConf.enabled = newOrg.allowCustom;
				timeConf.enabled = newOrg.specifyHours;
			}

			this.cachedOrg = newOrg;
		});

	}

	getPages() {
		return this.pageList.filter(t => t.enabled);
	}

	setPageIsDirty(dirtyStatus: boolean) {
		this.dirtyStatus = dirtyStatus;
		this.currentItemSource.next(this.dirtyStatus);
	}

	getPageDirtyStatus() {
		return Observable.create(observer => {
			//if we already have a valid object, add it to the stream
			if (this.dirtyStatus) observer.next(this.dirtyStatus);

			//watch the stream for updates
			const subsc = this.currentItemSource.subscribe(val => observer.next(val));

			//set up dispose
			return () => {
				if (subsc) subsc.unsubscribe();
			};
		});
	}
}
