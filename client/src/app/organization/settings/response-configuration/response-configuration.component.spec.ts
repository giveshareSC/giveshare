import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponseConfigurationComponent } from './response-configuration.component';

describe('ResponseConfigurationComponent', () => {
  let component: ResponseConfigurationComponent;
  let fixture: ComponentFixture<ResponseConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponseConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
