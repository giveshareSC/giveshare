import { Component} from '@angular/core';
import {SettingPageBase} from '../setting-page-base';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
	selector: 'app-response-configuration',
	templateUrl: './response-configuration.component.html',
	styleUrls: ['./response-configuration.component.css']
})
export class ResponseConfigurationComponent extends SettingPageBase {
	compTitle = 'Response Configuration';

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}

	specify() {
		this.organization.specifyHours = true;
		console.log('inside specify code', this.organization.specifyHours);
	}

	allday() {
		this.organization.specifyHours = false;
	}

	allowMultipleRedeems(allow) {
		this.organization.allowMultipleRedeems = allow;
	}
}
