import { OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models';
import {OrganizationApi} from '../../shared/sdk/services/custom';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {PageTitleService} from '../../page-title/page-title.service';
import {CurrentOrganizationService} from '../../shared/current-item.service';
import {SetupService} from './setup.service';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/operator/take';

export class SettingPageBase implements OnInit {

	organization = new Organization();
	compTitle = '';

	isDirty = false;

	constructor(protected orgApi: OrganizationApi, protected flashMessageService: FlashMessageService,
				protected titleService: PageTitleService, protected currentOrgSrv: CurrentOrganizationService,
				protected setupSrv: SetupService, protected route: ActivatedRoute, protected router: Router) {}

	ngOnInit() {
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.currentOrgSrv.getCurrentItem().take(1).subscribe(org => {
			this.organization = new Organization(org);
			//console.log('org data:', this.organization);
			// this.setupSrv.setPageIsDirty(true);
		});
		this.titleService.setTitle(this.compTitle);
	}

	save() {
		if (this.validate()) {
			this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {

				this.currentOrgSrv.setCurrentItemObj(result);
				this.setupSrv.setPageIsDirty(false);
				this.isDirty = false;
				this.flashMessageService.showSuccess('Setup information updated successfully');

				const thisPage = this.router.url.match(/([^\/]+)$/gi)[0];
				this.goToNextSettingPage(thisPage);
			}, err => {
				console.log('edit error', err);
				this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
			});
		}
	}

	goToNextSettingPage(thisPage) {
		const pageList = this.setupSrv.getPages();

		const nextPageIndex = pageList.findIndex(p => p.path === thisPage) + 1;

		if (0 <= nextPageIndex && nextPageIndex < pageList.length && !this.organization.setupComplete) {
			this.router.navigate(['/setup/' + this.organization.id + '/' + pageList[nextPageIndex].path]);
		} else {
			//EVERYTHING done!!
			console.log('EVERYTHING done!!');
			this.router.navigate(['/home/' + this.organization.id]);
		}
	}

	onChange() {
		this.setupSrv.setPageIsDirty(true);
		this.isDirty = true;
	}

	validate() {
		return true;
	}
}
