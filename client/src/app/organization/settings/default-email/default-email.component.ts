import { Component, OnInit } from '@angular/core';
import {Organization} from '../../../shared/sdk/models';
import {OrganizationApi} from '../../../shared/sdk/services/custom';
import {FlashMessageService} from '../../../flash-message/flash-message.service';
import {PageTitleService} from '../../../page-title/page-title.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SettingPageBase} from '../setting-page-base';
import {CurrentOrganizationService} from '../../../shared/current-item.service';
import {SetupService} from '../setup.service';


@Component({
	selector: 'app-default-email',
	templateUrl: './default-email.component.html',
	styleUrls: ['./default-email.component.css']
})
export class DefaultEmailComponent extends SettingPageBase {

	compTitle = 'Configure Email Content';
	editemail = false;

	constructor(o: OrganizationApi, f: FlashMessageService, t: PageTitleService, co: CurrentOrganizationService, s: SetupService, ar: ActivatedRoute, r: Router) {
		super(o,f,t,co,s,ar,r);
	}

	editEmail() {
		this.editemail = true;
	}

	saveEmail() {
		this.editemail = false;
		this.save();
	}
}
