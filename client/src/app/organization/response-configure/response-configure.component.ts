import { Component, OnInit } from '@angular/core';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {Organization} from '../../shared/sdk/models/Organization';
//import {OrganizationCouponOptionApi, OrganizationCouponOption, EmailTemplate, EmailTemplateApi} from '../../shared/sdk/';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {PageTitleService} from '../../page-title/page-title.service';

//import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'app-response-configure',
	templateUrl: './response-configure-sc.component.html',
	styleUrls: ['./response-configure.component.css']
})
export class ResponseConfigureComponent implements OnInit {

	organization = new Organization();
	saleslenhours = true;
	sethours = false;
	timetosethours = false;
	coupon = false;
	edit = false;
	TC = false;
	editTC = false;
	editemail = false;
	email = false;
	editinvite = false;
	invite = false;

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute,
				private flashMessageService: FlashMessageService,
				titleService: PageTitleService) {
		titleService.setTitle('Setup');
	}

	ngOnInit() {
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.orgApi.findById(params['orgId']).subscribe((org: Organization) => {
					this.organization = org;
					console.log("Hey, listen!" + this.organization.id);
				});
			} else {
				this.router.navigateByUrl('/');
			}
		});
	}

	specify() {
		this.organization.specifyHours = true;
		this.timetosethours = true;
		console.log('inside specify code', this.organization.specifyHours);
	}

	allday() {
		this.organization.specifyHours = false;
	}

	saveConfiguration() {
		this.sethours = true;
		this.saleslenhours = false;
		console.log('specific hours?', this.organization.specifyHours);
		if (this.organization.specifyHours !== true) {
			this.email = true;
			console.log('in false sHours code');
			this.sethours = false;
		}

		console.log('coupon', this.coupon);
		console.log('set hours', this.sethours);
		console.log('specific hours?', this.organization.specifyHours);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			this.router.navigateByUrl('/configure-response/' + this.organization.id);
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	saveConfigurationH() {
		const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
		const isValid = Array.prototype.concat.apply(
			[],
			days.map(d => [`${d}1`, `${d}2`])
		).some(d => this.organization[`${d}FromTime`] && this.organization[`${d}ToTime`]);

		if (isValid) {
			this.email = true;
			this.sethours = false;
			this.organization.specifyHours = true;

			console.log('coupon', this.coupon);
			console.log('set hours', this.sethours);
			console.log('specific hours?', this.organization.specifyHours);
			this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
				this.router.navigateByUrl('/configure-response/' + this.organization.id);
			}, err => {
				console.log('edit eror', err);
				this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
			});
		} else {
			this.flashMessageService.showError('Please supply at least one set of start and stop times.');
		}
	}

	editCoupon() {
		this.edit = true;
		this.coupon = false;
	}

	saveContent() {
		this.coupon = false;
		this.edit = false;
		this.TC = false;
		this.invite = true;
		console.log('specific hours?', this.organization.specifyHours);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			this.router.navigateByUrl('/configure-response/' + this.organization.id);
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	editInvite() {
		this.coupon = false;
		this.edit = false;
		this.TC = false;
		this.invite = false;
		this.editinvite = true;
		console.log('specific hours?', this.organization.specifyHours);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			this.router.navigateByUrl('/configure-response/' + this.organization.id);
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	saveInvite() {
		this.coupon = false;
		this.edit = false;
		this.TC = false;
		this.invite = false;
		this.editinvite = false;
		this.organization.setupComplete = true;
		console.log('specific hours?', this.organization.specifyHours);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			this.flashMessageService.showMessage({message: 'You are all set to roll! You can look for emails as you indicated letting you ' +
			'know how GiveShare is working for you!', messageClass: 'success'});

			//If there is a renewal date, then this is just an edit and we can return them home
			if (this.organization.subscriptionRenewalDate) {
				this.router.navigateByUrl('/home/' + this.organization.id);
			} else {
				//if there is no renewal date, then this is a new setup, and we need to kick them to subscription
				this.router.navigateByUrl('/subscription/' + this.organization.id);
			}
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	saveTC() {
		this.TC = false;
		this.coupon = true;
		console.log('specific hours?', this.organization.specifyHours);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			this.router.navigateByUrl('/configure-response/' + this.organization.id);
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	editEmail() {
		this.email = false;
		this.editemail = true;
	}

	saveEmail() {
		this.coupon = false;
		this.editemail = false;
		this.email = false;
		this.TC = true;
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {
			// this.router.navigateByUrl('/home/' + this.organization.id);
			this.router.navigateByUrl('/configure-response/' + this.organization.id);
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}
/*
		console.log('templates', this.templates);
		const saveMap = [];

		if (this.responseOption.id && this.responseOption.id > 0) {
			saveMap.push(this.orgOptApi.replaceById(this.responseOption.id, this.responseOption));
		} else {
			delete this.responseOption.id;
			saveMap.push(this.orgOptApi.create(this.responseOption).map(r => {
				this.responseOption.id = r.id;
				return r;
			}));
		}

		this.templates.forEach(t => {
			if (t.id && t.id > 0) {
				saveMap.push(this.emailTempApi.replaceById(t.id, t));
			} else {
				delete t.id;
				saveMap.push(this.emailTempApi.create(t).map(r => {
					t.id = r.id;
					return r;
				}));
			}
		});

		Observable.forkJoin(saveMap).subscribe(res => {
			console.log('things saved', res);
			this.flashMessage.showMessage({messageClass: 'success', message: 'Options saved successfully.'});
			this.router.navigateByUrl('/dashboard/' + this.organizationId);
		});
	}*/

}
