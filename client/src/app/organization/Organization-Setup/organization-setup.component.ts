import {Component, OnDestroy, OnInit} from '@angular/core';
import {Organization} from '../../shared/sdk/models';
import {OrganizationApi} from '../../shared/sdk/services/custom';
import {ActivatedRoute, Router} from '@angular/router';
import {PageTitleService} from '../../page-title/page-title.service';
import {CurrentOrganizationService} from '../../shared/current-item.service';
import {SetupService} from '../settings/setup.service';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {Subscription} from 'rxjs/Subscription';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

class SettingPage {
	name: string;
	path: string;
	enabled: boolean;

	constructor(name: string, path: string, enabled = true) {
		this.name = name;
		this.path = path;
		this.enabled = enabled;
	}
}

@Component({
	selector: 'app-org-setup',
	templateUrl: './organization-setup.component.html',
	styleUrls: ['./organization-setup.component.css']
})
export class OrgSetupComponent implements OnInit, OnDestroy {
	dirtyStatusSubscription: Subscription;
	currentOrgSubscription: Subscription;

	organization = new Organization();
	currentStep = 0;

	pageIsDirty = false;

	constructor(private orgApi: OrganizationApi, private route: ActivatedRoute, private router: Router,
				titleService: PageTitleService, private currentOrgService: CurrentOrganizationService,
				public setupService: SetupService, private flashMessage: FlashMessageService) {
		//titleService.setTitle('Setup Account');
	}

	ngOnInit() {
		//get requested organization
		this.route.params
			.map(params => params['orgId'])
			.filter(orgId => orgId)
			.subscribe(orgId => {
				this.currentOrgService.setCurrentItem(orgId);
			});

		this.dirtyStatusSubscription = this.setupService.getPageDirtyStatus().subscribe(status => this.pageIsDirty = status);

		//console.log('route', this.route.snapshot.children[0].url)//_routerState.url);
		//On load, look up what child is currently on screen
		if (this.route.snapshot.children && this.route.snapshot.children.length) {
			this.currentStep = this.setupService.getPages().findIndex(p => p.path === this.route.snapshot.children[0].url[0].path);
		} else {
			this.currentStep = -1;
		}


		//watch child components being changed
		const urlMatcher = /\/setup\/\w+\/([\w_-]+)/;
		this.router.events.subscribe((event: any) => {
			//console.log('got url', event.url, event.state);
			const parts = urlMatcher.exec(event.url);
			if (parts && parts.length === 2) {
				//console.log('got url match', parts);
				this.currentStep = this.setupService.getPages().findIndex(p => p.path === parts[1]);
			}
		});
	}

	ngOnDestroy() {
		if (this.dirtyStatusSubscription && !this.dirtyStatusSubscription.closed) {
			this.dirtyStatusSubscription.unsubscribe();
		}
		if (this.currentOrgSubscription && !this.currentOrgSubscription.closed) {
			this.currentOrgSubscription.unsubscribe();
		}
	}

	canShowNext() {
		return this.getNext() != null;
	}
	canShowPrev() {
		return this.getBack() != null;
	}

	getBack(): SettingPage {
		if (!this.currentStep || this.currentStep === -1) return null;
		const remaining = this.setupService.getPages().slice(0, this.currentStep);
		return remaining.length ? remaining[remaining.length - 1] : null;
	}

	getNext(): SettingPage {
		//console.log('steps', this.pageList.slice(this.currentStep + 1))
		return this.setupService.getPages().slice(this.currentStep + 1)[0];
	}

	goBack() {
		if (this.pageIsDirty) {
			this.flashMessage.showError('You have unsaved changes. Please save them before continuing');
			return;
		}
		const backItem = this.getBack();
		if (backItem) {
			this.router.navigate(['./' + backItem.path], { relativeTo: this.route });
		}
	}

	goNext() {
		if (this.pageIsDirty) {
			this.flashMessage.showError('You have unsaved changes. Please save them before continuing');
			return;
		}
		console.log('route', this.route);
		console.log('gonext');
		const nextItem = this.getNext();
		if (nextItem) {
			this.router.navigate(['./' + nextItem.path], { relativeTo: this.route });
		}
	}

	goHome() {
		if (this.pageIsDirty) {
			this.flashMessage.showError('You have unsaved changes. Please save them before continuing');
			return;
		}
		console.log('route', this.route);
		console.log('gohome');

		this.router.navigate(['/home/' + this.organization.id]);
	}

	getBackText() {
		const backItem = this.getBack();
		if (backItem) {
			return `${backItem.name}`;
		} else {
			return '';
		}
	}

	getNextText() {
		const nextItem = this.getNext();
		if (nextItem) {
			return `${nextItem.name}`;
		} else {
			return '';
		}
	}
}


