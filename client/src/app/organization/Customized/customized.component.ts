import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {PageTitleService} from '../../page-title/page-title.service';

@Component({
	selector: 'app-custom',
	templateUrl: './customized.component.html',
	styleUrls: ['./customized.component.css']
})
export class CustomizedComponent implements OnInit {

	private organizationId: string;
	custCamp = false;
	all = true;
	today = new Date();
	endDate = new Date();

	orgDefVals = {
		discount: 0,
		daysgoodfor: 0,
	};

	organization = new Organization();
	newCampaign = new Campaign();
	campaigns: Campaign[];


	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute, private campApi: CampaignApi,
				private flashMessageService: FlashMessageService, private auth: LoopBackAuth, titleService: PageTitleService) {
		titleService.setTitle('Requested Campaigns Management');
	}

	ngOnInit() {
		console.log('inside OnInit');
		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) {
			this.flashMessageService.showMessage({messageClass: 'danger', message: 'You are not logged in'});
			return;
		}

		this.organizationId = curUser.orgId;

		const filter = {
			include: {
				relation: 'campaigns',
				scope: {
					where: {
						status: 'requested'
					},
				},
			},
		};
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.organizationId = params['orgId'];
				console.log('looking for org', this.organizationId, filter);
				this.orgApi.findById(this.organizationId, filter).subscribe((org: Organization) => {
					this.organization = org;
					this.campaigns = org.campaigns;

					//if any of the campaigns are missing a discount percentage, default one
					this.campaigns.filter(c => typeof c.discountPercentage === 'undefined')
						.forEach(c => c.discountPercentage = 0);
					console.log('entered init code', this.campaigns);
				});
			} else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}

		});

	}

	goBack() {
		// this.all = true;
		this.custCamp = false;
	}

	goToExp() {
		this.router.navigateByUrl('/Expired/' + this.organizationId);
	}

	setCustom(campaign: Campaign) {
		console.log('editing user', campaign);
		this.newCampaign = campaign;
		this.custCamp = true;
		// this.all = false;
	}

	setRejected(campaign: Campaign) {
		if (confirm('Are you sure you want to reject this request from \'' + campaign.charityName + '\'')) {
			campaign.status = 'rejected';
			this.campApi.patchAttributes(campaign.id, campaign).subscribe(res => {
				console.log('rejected campaign');
				this.removeCampaignFromList(campaign);
			});
		}
	}

	sendCustom() {
		console.log('new discount', this.orgDefVals.discount);
		this.newCampaign.discountPercentage = this.orgDefVals.discount;
//		this.newCampaign = campaign;
		this.newCampaign.startDate = this.today;
		this.newCampaign.reqDate = this.today;
		this.newCampaign.status = 'active';

		// this.endDate = new Date(new Date().getTime() + ((this.orgDefVals.daysgoodfor) * 24 * 60 * 60 * 1000));
		this.endDate = new Date();
		this.endDate.setDate(this.endDate.getDate() + this.orgDefVals.daysgoodfor);
		this.endDate.setHours(23, 59, 59, 999);

		this.newCampaign.endDate = this.endDate;

		console.log('end date is now', this.newCampaign.endDate);
		this.campApi.patchAttributes(this.newCampaign.id, this.newCampaign).subscribe(res => {
			console.log('got save res', res);
			this.flashMessageService.showMessage({messageClass: 'success', message: 'Campaign approved with custom values.'});
			this.removeCampaignFromList(this.newCampaign);
			this.custCamp = false;
		});
		// this.ngOnInit();
	}

	sendDefault(campaign: Campaign) {

		this.newCampaign = campaign;
		this.newCampaign.startDate = this.today;
		this.newCampaign.reqDate = this.today;
		this.newCampaign.discountPercentage = this.organization.discountAmount;
		this.newCampaign.status = 'active';

		//this.endDate = new Date(new Date().getTime() + ((this.organization.daysValid) * 24 * 60 * 60 * 1000));
		this.endDate = new Date();
		this.endDate.setDate(this.endDate.getDate() + this.organization.daysValid);
		this.endDate.setHours(23, 59, 59, 999);

		this.newCampaign.endDate = this.endDate;

		console.log('end date is now', this.newCampaign.endDate);
		this.campApi.patchAttributes(this.newCampaign.id, this.newCampaign).subscribe(res => {
			console.log('got save res', res);
			this.flashMessageService.showMessage({messageClass: 'success', message: 'Campaign approved with default values.'});
			this.removeCampaignFromList(campaign);
			this.custCamp = false;
		});
		// this.router.navigateByUrl('/Customized/' + this.organizationId);
	}

	//remove a campaign from the list
	private removeCampaignFromList(campaign: Campaign) {
		//remove the campaign that we just modified from retrieved list
		this.campaigns = this.campaigns.filter(c => c.id !== campaign.id);
	}
}
