import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {Organization, OrganizationApi, CampaignDetailsApi} from '../../shared/sdk/';
import {CampaignDetails} from '../../shared/sdk/models/CampaignDetails';
import {PageTitleService} from '../../page-title/page-title.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';

@Component({
	selector: 'app-campaign-request',
	templateUrl: './campaign-request.component.html',
	styleUrls: ['./campaign-request.component.css']
})
export class CampaignRequestComponent implements OnInit {
	private campaign: Campaign;

	offerText = '';
	discount = '';
	emailAddr: string;
	private organizationId: string;
	private campaignId: string;

	constructor(private router: Router, private orgApi: OrganizationApi, titleService: PageTitleService,
				private flashMessageService: FlashMessageService, private  campDetsApi: CampaignDetailsApi,
				private route: ActivatedRoute, private auth: LoopBackAuth, private campApi: CampaignApi) {
		titleService.setTitle('Request Invitation');
	}

	ngOnInit() {
		// this.organizationId = curUser.orgId;
		// console.log('looking for org for default offer content', this.organizationId);
		// this.orgApi.findOne({where: {id: this.organizationId}}).subscribe((org: Organization) => {
		// 	this.organization = org;
		this.route.params
			.map(params => params['campaignId'])
			.filter(campId => campId)
			.flatMap(campaignId => {
				console.log('looking for org for campaign lookup', campaignId);
				this.campaignId = campaignId;
				return this.campApi.getPublicData(campaignId);
			}).subscribe((campaign: Campaign) => {
				this.campaign = campaign;
				this.organizationId = campaign.organizationId;
				this.discount = String(campaign.discountPercentage);
				this.discount = this.discount + '% ';

				if (this.campaign.status !== 'active') {
					this.flashMessageService.showError('So sorry!  This campaign has already expired!!');
					this.router.navigateByUrl('/');
				}

				const campaignEnd = new Date(campaign.endDate);

				this.offerText = campaign.organization.defOfferContent
					.replace(/\{\{charity-name\}\}/g, campaign.charityName)
					.replace(/\{\{discount-percent\}\}/g, this.discount)
					.replace(/\{\{campaign.charityName\}\}/g, campaign.charityName)
					.replace(/\{\{campaign-end\}\}/g, `${campaignEnd.getMonth() + 1}/${campaignEnd.getDate()}/${campaignEnd.getFullYear()}`);
				// 	.replace(/\{\{organization-name\}\}/, this.name)
				// 	.replace(/\{\{organization-address\}\}/, `${this.address}\n${this.city}, ${this.state} ${this.zip}`)
				// 	.replace(/\{\{organization-street-address\}\}/, this.address)
				// 	.replace(/\{\{organization-city\}\}/, this.city)
				// 	.replace(/\{\{organization-state\}\}/, this.state)
				// 	.replace(/\{\{organization-zip\}\}/, this.zip)
				// 	.replace(/\{\{campaign-url\}\}/, `http://giveshare.io/${this.slug}/${campaignId}`);
			});
	}

	reqCoupon() {
		if (!this.emailAddr) {
			this.flashMessageService.showError('Please provide an email address');
			return;
		}

		this.campApi.requestCoupon(this.campaignId, this.emailAddr).subscribe(res => {
			if (res.success) {
				this.flashMessageService.showSuccess(res.message);
				this.router.navigateByUrl('/');
			} else {
				this.flashMessageService.showError(res.message);
			}
		});
	}
}
