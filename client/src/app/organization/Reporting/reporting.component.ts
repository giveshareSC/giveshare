import { Component, OnInit } from '@angular/core';
import {Campaign, CampaignDetails, Organization, OrganizationApi, CampaignApi, CampaignDetailsApi} from '../../shared/sdk/';
import {ActivatedRoute, Router} from '@angular/router';
//import {Observable} from 'rxjs/Observable';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {PageTitleService} from '../../page-title/page-title.service';
import {exportCsv} from '../../utilities';

//import {CampaignInterface} from '../campaign';

@Component({
	selector: 'app-campaign-reports',
	templateUrl: './reporting.component.html',
	styleUrls: ['./reporting.component.css']
})
export class CampaignReportingComponent implements OnInit {

	compTitle = {
		title: 'Reporting'
	};

	searchVals = {
		name: '',
		status: '',
		startRangeStart: null,
		startRangeEnd: null,
		endRangeStart: null,
		endRangeEnd: null,
	};

	organization = new Organization();
	newSales = 0;
	oldSales = 0;
	noDetails = true;
	Details = false;
	searched = false;
	editrec = false;
	campaign = new Campaign();
	campaigns: Campaign[];
	details: CampaignDetails[];
	recDetails = new CampaignDetails();
	options: { id: number, percent: string, value: number }[];
//	campaigns: ResponsePageCampaign[];


	private organizationId: string;
	private CampaignId: string;

	constructor(private router: Router, private route: ActivatedRoute, private campApi: CampaignApi, private campdetApi: CampaignDetailsApi,
				private flashMessageService: FlashMessageService, private orgApi: OrganizationApi, private auth: LoopBackAuth,
				titleService: PageTitleService) {
		titleService.setTitle('Campaign Reporting');
		}

	ngOnInit() {

	}

	searchCampaigns() {

		const curUser: CustomUser = this.auth.getCurrentUserData();
		if (!curUser || !curUser.orgId) {
			this.flashMessageService.showMessage({messageClass: 'danger', message: 'You are not logged in'});
			return;
		}
		this.Details = false;

		this.organizationId = curUser.orgId;
		// this.route.params.subscribe(params => {
		// 	if (params['orgId']) {
		// 		console.log('looking for org for campaign lookup', this.organizationId);
		// 		this.orgApi.findOne({where: {id: params['orgId']}}).subscribe((org: Organization) => {
		// 			this.organization = org;
		// 		});
		// 	} else {
		// 		console.log('no org found', this.organization);
		// 		this.router.navigateByUrl('/');
		// 	}
		// });
		const filter = {
			where: {} as any
		};
		if (this.searchVals.name) {
			filter.where.charityName = {
				regexp: `.*${this.searchVals.name}.*`,
			};
		}

		//handle start date
		if (this.searchVals.startRangeStart && this.searchVals.startRangeEnd) {
			filter.where.startDate = {
				between: [this.searchVals.startRangeStart, this.searchVals.startRangeEnd],
			};
		} else if (this.searchVals.startRangeStart) {
			filter.where.startDate = {
				gte: this.searchVals.startRangeStart
			};
		} else if (this.searchVals.startRangeEnd) {
			filter.where.startDate = {
				lte: this.searchVals.startRangeEnd
			};
		}


		//handle end date
		if (this.searchVals.endRangeStart && this.searchVals.endRangeEnd) {
			filter.where.endDate = {
				between: [this.searchVals.endRangeStart, this.searchVals.endRangeEnd],
			};
		} else if (this.searchVals.endRangeStart) {
			filter.where.endDate = {
				gte: this.searchVals.endRangeStart
			};
		} else if (this.searchVals.endRangeEnd) {
			filter.where.endDate = {
				lte: this.searchVals.endRangeEnd
			};
		}

		console.log('looking up campaigns', this.organizationId);
		this.orgApi.getCampaigns(this.organizationId, filter).subscribe(camps => {
			this.campaigns = camps;
			});

		this.searched = true;
	}

	detailScreen(campaign: Campaign) {
		this.Details = true;
		this.noDetails = false;
		this.campaign = campaign;
		this.searched = false;
		console.log('looking up details', this.campaign.id);
		this.campApi.getDetails(this.campaign.id, {include: [
			{relation: "redeemingOrg", scope: {fields: ["id", "name"]}},
			{relation: "organization", scope: {fields: ["id", "name"]}},
			{relation: "campaign", scope: {fields: ["id", "discountPercentage"]}},
		]}).subscribe(deets => {
			this.details = deets;
			this.details.forEach(d => {
				d.donationAmt = d.TotalSales * ((d.campaign.discountPercentage) / 100);
			})
		});
		this.router.navigateByUrl('/Reporting/' + this.organization.id);
	}

	goBack() {
		this.Details = false;
		this.noDetails = true;
		this.searchCampaigns();
	}

	goBacktoDets() {
		this.Details = true;
		this.noDetails = false;
		this.editrec = false;
		this.searchCampaigns();
	}

	editRecord(detail: CampaignDetails) {
		console.log('editing coupon', detail);
		console.log('is this better?', this.details);
		this.recDetails = detail;
		this.oldSales = this.recDetails.TotalSales;
		this.editrec = true;
		this.Details = false;
	}

	updateCoupon() {

				console.log('looking to update coupon', this.recDetails.id);
				this.campdetApi.patchAttributes(this.recDetails.id, this.recDetails).subscribe(result => {

					this.flashMessageService.showMessage({
						message: 'You have updated the record!!',
						messageClass: 'success'
					});
					this.editrec = false;
					this.Details = true;
					this.router.navigateByUrl('/Reporting/' + this.organizationId);
					this.CampaignId = this.recDetails.CampaignId;

					this.campApi.findById(this.CampaignId).subscribe((camps: Campaign) => {
						this.campaign = camps;
						console.log('current camp amt', this.campaign.resultingAmount);
						console.log('details $$ adjusted:', this.recDetails.TotalSales);
						console.log('trying to subtract:', this.oldSales);
						this.newSales = (this.campaign.resultingAmount - this.oldSales);
						this.newSales = (this.newSales + this.recDetails.TotalSales);
						console.log('newSales var:', this.newSales);
						this.campaign.resultingAmount = this.newSales;

						this.campaign.resultingAmount = this.newSales;

						this.campApi.patchAttributes(this.CampaignId, this.campaign).subscribe(res => {
							console.log('updated amount:', this.campaign.id, this.campaign.resultingAmount);
						}, err => {
							console.log('cant update campaign', this.CampaignId, err);
						});
					}, err => {
						console.log('cant find campaign', this.CampaignId);
					});
				});
	}

	exportCSV(redemptionListTable: HTMLTableElement) {
		exportCsv(redemptionListTable);
	}
}
