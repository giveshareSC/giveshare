import { Component, OnInit } from '@angular/core';
import {Campaign, OrganizationApi, CampaignApi} from '../../shared/sdk/';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {LoopBackAuth} from '../../shared/sdk/services/core/auth.service';
import {CustomUser} from '../../shared/sdk/models/CustomUser';
import {PageTitleService} from '../../page-title/page-title.service';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
// import {CampaignInterface} from '../campaign';

@Component({
	selector: 'app-campaign-response',
	templateUrl: './campaign-response-done.component.html',
	styleUrls: ['./campaign-response.component.css']
})
export class CampaignResponseComponent implements OnInit {

	campaignStatus = 'new';
	campaigns: ResponsePageCampaign[];


	private organizationId: string;

	constructor(protected auth: LoopBackAuth, private router: Router, private orgApi: OrganizationApi, private campApi: CampaignApi,
				private route: ActivatedRoute, private flashMessage: FlashMessageService, titleService: PageTitleService) {
		titleService.setTitle('Respond');
	}

	ngOnInit() {
		if (this.route.snapshot.data && this.route.snapshot.data['status']) {
			this.campaignStatus = this.route.snapshot.data['status'];
		}

		const curUser: CustomUser = this.auth.getCurrentUserData();

		if (!curUser || !curUser.orgId) return;

		//TODO use route params
		this.organizationId = curUser.orgId;

		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.organizationId = params['orgId'];
				const filter = { where: { } as any, };
				switch (this.campaignStatus) {
					case 'new':
						filter.where.resultingAmount = null;
						filter.where.isRead = false;
						break;
					case 'read':
						filter.where.resultingAmount = null;
						filter.where.isRead = true;
						break;
					case 'completed':
						filter.where.resultingAmount = {neq: null};
						break;
				}

				this.orgApi.getCampaigns(this.organizationId, filter).subscribe(camps => this.campaigns = camps);
			}  else {
				console.log('no org found', params['orgId']);
				this.router.navigateByUrl('/');
			}
		});
	}

	getName(option) {
		const isPercent = option.percent === 'true';
		return (!isPercent ? '$' : '') + option.value + (isPercent ? '%' : '');
	}

	readCampaign(campaign) {
		campaign.expanded = true;

		if (!campaign.isRead) {
			this.campApi.patchAttributes(campaign.id, {isRead: true}).subscribe(s => {
				console.log('marked as read');
			});
		}
	}

	sendResponses() {
		const isComplete = this.campaigns.every(c => !!c.selectedOption);

		if (isComplete) {
			console.log('send', this.campaigns);
			const saveObjs = this.campaigns.map(c => {
				return this.campApi.patchAttributes(c.id, {resultingAmount: 1/*c.resultingAmount*/})
					.flatMap(r => {
						return this.orgApi.sendResponse(
							this.organizationId,
							c.selectedOption === '0' ? 'rejected' : 'granted',
							c.id,
							c.selectedOption
						);
					});
			});
			Observable.forkJoin(saveObjs).subscribe((responses) => {
				this.flashMessage.showMessage({message: `${responses.length} responses sent`, messageClass: 'success'});
				this.campaigns = [];
			}, err => {
				this.flashMessage.showMessage({message: 'error sending responses', messageClass: 'danger'});
			});
		} else {
			this.flashMessage.showMessage({message: 'Please supply a response value for all requests.', messageClass: 'danger'});
		}
	}
}


@Component({
	selector: 'app-completed-campaign-response',
	templateUrl: './campaign-response-done.component.html',
	styleUrls: ['./campaign-response.component.css']
})
export class CompletedCampaignResponseComponent extends CampaignResponseComponent {
	campaignStatus = 'completed';
}

export class ResponsePageCampaign extends Campaign {
	selectedOption: string;
	expanded: false;
}
