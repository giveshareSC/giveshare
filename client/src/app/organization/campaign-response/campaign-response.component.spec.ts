import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignResponseComponent } from './campaign-response.component';

describe('CampaignResponseComponent', () => {
  let component: CampaignResponseComponent;
  let fixture: ComponentFixture<CampaignResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
