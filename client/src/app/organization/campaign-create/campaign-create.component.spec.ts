import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CampaignCreateComponent} from './campaign-create.component';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

describe('CampaignCreateComponent', () => {
	let component: CampaignCreateComponent;
	let fixture: ComponentFixture<CampaignCreateComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CampaignCreateComponent],
			imports: [FormsModule],
			providers: [
				{provide: ActivatedRoute, useValue: {
					params: Observable.of({campaign: 'test'})
				}},
				{provide: Router, useValue: {
					navigateByUrl: jasmine.createSpy('navigateByUrl')
				}},
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CampaignCreateComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
