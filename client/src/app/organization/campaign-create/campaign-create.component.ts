import { Component, OnInit } from '@angular/core';
// import {CampaignInterface, OrganizationInterface} from '../campaign';
import {CampaignApi, CampaignInterface, OrganizationInterface, Organization, OrganizationApi} from '../../shared/sdk/';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {HeaderService} from '../../header/header.service';

@Component({
	selector: 'app-org-create-component',
	templateUrl: './campaign-create.component.html',
	styleUrls: ['./campaign-create.component.css']
})
export class CampaignCreateComponent implements OnInit {

	organization: OrganizationInterface;

	orgIsUpToDate = false;

	newCampaign: CampaignInterface = {
		organizationId: null,
		pointOfContactEmail: null,
		pointOfContactName: null,
		charityName: null,
		description: null,
		status: null,
		mailingAddress: null,
		mailingCity: null,
		mailingState: null,
		mailingZip: null,
		resultingAmount: null,
		w9Reference: null,
	};

	constructor(private route: ActivatedRoute, private router: Router, private organizationApi: OrganizationApi,
		private campaignApi: CampaignApi, private flashMessage: FlashMessageService, headerService: HeaderService) {
		headerService.setHeader(false);
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params['campaign']) {
				this.organizationApi.getPublicDataBySlug(params['campaign']).subscribe((org: Organization) => {
					this.organization = org;
					this.newCampaign.organizationId = org.id;

					this.orgIsUpToDate = (<any> org).isActive;
				});
			} else {
				this.router.navigateByUrl('/');
			}
		});
	}

	submitApplication() {
		this.campaignApi.create(this.newCampaign).subscribe(res => {
			console.log('got save res', res);
			this.flashMessage.showMessage({messageClass: 'success', message: 'Donation request submitted.'});

			setTimeout(() => {
				//redirect the user after 2 seconds
				this.router.navigateByUrl('/');
			}, 2000);
		});
	}
}
