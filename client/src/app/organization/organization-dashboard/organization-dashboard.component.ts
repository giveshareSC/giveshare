import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';

@Component({
	selector: 'app-organization-dashboard',
	templateUrl: './organization-dashboard.component.html',
	styleUrls: ['./organization-dashboard.component.css']
})
export class OrganizationDashboardComponent implements OnInit {
	organization = new Organization();

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute,
				private flashMessageService: FlashMessageService) { }

	ngOnInit() {
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.orgApi.findById(params['orgId']).subscribe((org: Organization) => {
					this.organization = org;
				});
			} else {
				this.router.navigateByUrl('/');
			}
		});
	}

}
