import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {CampaignApi} from '../../shared/sdk/services/custom/Campaign';
import { DatePipe } from '@angular/common';
import {Organization, OrganizationApi, CampaignDetailsApi} from '../../shared/sdk/';
import {CampaignDetails} from '../../shared/sdk/models/CampaignDetails';
import {Campaign} from '../../shared/sdk/models/Campaign';
import {PageTitleService} from '../../page-title/page-title.service';

@Component({
	selector: 'app-campaign-redemption',
	templateUrl: './campaign-redemption.component.html',
	styleUrls: ['./campaign-redemption.component.css'],
	providers: [DatePipe]
})

export class CampaignRedemptionComponent implements OnInit {

	details = new CampaignDetails();
	organization = new Organization();
	campaign = new Campaign();
	campaigns: Campaign[];
	validtime = false;
	validate = false;
	needMgrCode = false;
	outOfTimeMsg = false;
	redeem = true;
	day = null;
	military = new Date();

	Passed = {
		redemptionCode: 0,
		limit: 0
	};
	daysValidString: string[] = [];

	constructor(private router: Router, private orgApi: OrganizationApi, private campApi: CampaignApi,
				private datePipe: DatePipe, private titleService: PageTitleService,
				private flashMessageService: FlashMessageService, private  campDetsApi: CampaignDetailsApi,
				private route: ActivatedRoute) {
		titleService.setTitle('Redemption!');
	}

	ngOnInit() {
		this.validtime = false;
		this.route.params.subscribe(params => {
			if (params['CampaignDetailsId']) {
				this.details.id = params['CampaignDetailsId'];
				console.log('CDID', params['CampaignDetailsId']);
				this.campDetsApi.getPublicData(params['CampaignDetailsId']).subscribe((deets: CampaignDetails) => {
					this.details = deets;
					this.organization = deets && deets.organization;
					this.campApi.getPublicData(this.details.CampaignId).subscribe(camp => this.campaign = camp);
					this.daysValid();

					console.log('checking for already redeemed', this.details.id);

					if (this.details.RedeemedDate != null && !this.organization.allowMultipleRedeems) {
						this.flashMessageService.showMessage({
							message: 'This invitation has already been redeemed!!',
							messageClass: 'danger'
						});
						this.router.navigateByUrl('/');
					}

				}, err => {
					console.log('edit eror', err);
					this.flashMessageService.showMessage({message: 'Could not redeem', messageClass: 'danger'});
				});
			}
		});
	}

	redCoupon() {
		console.log('passed code!!', this.Passed.redemptionCode);
		// console.log('org code:', this.organization.redemptionCode);

		this.campDetsApi.redeem(this.details.id, this.details.TotalSales, this.details.TotalGuests, this.Passed.redemptionCode).subscribe(res => {
			console.log('redeem result', res);
			if (res.success) {
				this.flashMessageService.showMessage({
					message: 'Thank you for your support!!',
					messageClass: 'success'
				});
				this.router.navigateByUrl('/');
			} else if (res.message === 'Time') {
				this.outOfTimeMsg = true;
				this.titleService.setTitle('Please come back later');
			} else {
				this.needMgrCode = true;
				this.flashMessageService.showMessage({
					message: res.message,
					messageClass: 'danger'
				});
				//if (res.message === '') {
				this.Passed.redemptionCode = 0;
				this.router.navigateByUrl('/redeem/' + this.details.id);
				//}
			}
		});
	}

	checkValidity() {
		if (this.redeem) {
			this.redeem = false;
			this.validate = true;
		}

		this.details.RedeemedDate = new Date();
		this.military = new Date();
		this.day = this.details.RedeemedDate.getDay();

	}

	daysValid() {
		const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];

		days.forEach(day => {
			if (this.organization[`${day}1FromTime`]) this.daysValidString.push(`${day} ${this.organization[`${day}1FromTime`]} - ${this.organization[`${day}1ToTime`]}`);
			if (this.organization[`${day}2FromTime`]) this.daysValidString.push(`${day} ${this.organization[`${day}2FromTime`]} - ${this.organization[`${day}2ToTime`]}`);
		})
	}
}
