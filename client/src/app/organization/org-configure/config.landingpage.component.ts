import { Component, OnInit } from '@angular/core';
import {Organization} from '../../shared/sdk/models/Organization';
import {OrganizationApi} from '../../shared/sdk/services/custom/Organization';
import {ActivatedRoute, Router} from '@angular/router';
import {FlashMessageService} from '../../flash-message/flash-message.service';
import {HeaderService} from '../../header/header.service';

declare const filestack: {
	init(apiKey: string): {
		pick({ maxFiles }: { maxFiles: number }):
			Promise<{ filesUploaded: { url: string }[] }>
	}
};

@Component({
	selector: 'app-org-configure',
	templateUrl: './config.landingpage.component.html',
	styleUrls: ['./config.landingpage.component.css']
})
export class OrgConfigureComponent implements OnInit {

	readonly acceptableCharacters = /^[A-Za-z0-9_-]+$/;

	organization = new Organization();

	isSlugValidationPending = false;
	isSlugValid= true;
	slugMessage = '';

	constructor(private orgApi: OrganizationApi, private router: Router, private route: ActivatedRoute,
				private flashMessageService: FlashMessageService, headerService: HeaderService) {
		headerService.setHeader(false);
	}

	ngOnInit() {
		//TODO pull org id from user
		// this.orgApi.findById(1).subscribe((o: Organization) => this.organization = o);
		this.route.params.subscribe(params => {
			if (params['orgId']) {
				this.orgApi.findById(params['orgId']).subscribe((org: Organization) => {
					this.organization = org;
				});
			} else {
				this.router.navigateByUrl('/');
			}
		});
	}

	slugChanged() {
		//trim the slug in case the use accidentally left a space at the end
		this.organization.slug = (this.organization.slug || '').trim();

		//if the slug has a space, etc., throw an error
		if (!this.acceptableCharacters.test(this.organization.slug)) {
			this.isSlugValidationPending = false;
			this.isSlugValid = false;
			this.slugMessage = 'The slug can only contain numbers, letters, "_", and "-"';
		} else {
			//check the API to see if there is already an org with that slug
			this.isSlugValidationPending = true;
			this.slugMessage = 'Validation Pending';
			//TODO debounce
			this.orgApi.getPublicDataBySlug(this.organization.slug).subscribe(org => {
				this.isSlugValidationPending = false;
				this.isSlugValid = org === null || !org.id || org.id === this.organization.id;
				this.slugMessage = this.isSlugValid ? 'Slug is valid' : 'Sorry, that slug is in use';
			});
		}
	}

	updateOrg() {
		if (this.isSlugValidationPending || !this.isSlugValid) {
			return;
		}

		console.log(this.organization);
		this.orgApi.patchAttributes(this.organization.id, this.organization).subscribe(result => {

			this.flashMessageService.showMessage({message: 'Landing Page information updated successfully', messageClass: 'success'});

			if(!this.organization.setupComplete) {
				this.router.navigate(['/setup/' + this.organization.id]);
			}
		}, err => {
			console.log('edit eror', err);
			this.flashMessageService.showMessage({message: 'Invalid organization', messageClass: 'danger'});
		});
	}

	async showPicker() {
		const client = filestack.init('AOkwl2T1TwGjNpJEXRC3wz');
		const result = await client.pick({ maxFiles: 1 });
		console.log('res', result);
		console.log('file', result.filesUploaded[0]);
		const url = result.filesUploaded[0].url;
		this.organization.logo = url;
	}

}
