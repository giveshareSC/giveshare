import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OrgConfigureComponent} from './config.landingpage.component';

describe('OrgConfigureComponent', () => {
	let component: OrgConfigureComponent;
	let fixture: ComponentFixture<OrgConfigureComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [OrgConfigureComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(OrgConfigureComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
