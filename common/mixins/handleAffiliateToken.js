module.exports = function(Model, options) {
	/*
	* Before we create an object, check to see if there is an affiliate token cookie.
	* If there is, attach this to the model.
	 */
	Model.beforeRemote('create', (context, model, next) => {
		const affiliateMatches = /(;\s*)?affiliate_token=(\w+)(;|\s|$)/.exec(context.req.get('cookie') || '');
		const availableProperties = Object.keys((Model.definition && Model.definition.properties) || {});

		if (affiliateMatches && affiliateMatches.length > 2) {
			const tokenId = affiliateMatches[2];
			return Model.app.models.AffiliateToken.findById(tokenId).then(token => {
				// console.log('token', token);
				if (token) {
					//check if we need the affiliateToken property
					if (availableProperties.includes('affiliateToken')) {
						context.args.data.affiliateToken = token.id;
					}

					//check if we need to set the affiliate column
					if (availableProperties.includes('affiliate')) {
						context.args.data.affiliate = token.affiliate;
					}

					console.log('set token', context.req.body);
				}
				return true;
			}).catch(err => {
				console.log('token lookup failed', err);
				return false;
			});
		} else {
			return Promise.resolve(true);
		}
	});
};
