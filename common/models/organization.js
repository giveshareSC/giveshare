module.exports = function(Organization) {
	const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];

	Organization.observe('before save', function(ctx, next) {
		if (!ctx.isNewInstance) {
			return Promise.resolve();
		}
		const instance = ctx.instance;

		//Validate the slug
		//>>OK.  You are a good slug, and a pretty slug, and...
		//No, I mean validate that the slug is *unique*
		//>>Oh...
		if (!instance.slug) {
			instance.slug = instance.name.replace(new RegExp('[^A-Za-z0-9_-]', 'g'), '').toLowerCase();
		} else {
			instance.slug = instance.slug.replace(new RegExp('[^A-Za-z0-9_-]', 'g'), '').toLowerCase();
		}
		return Organization.getUniqueSlug(instance).then(promise => {
			return true;
		});

		//TODO enable this after testing
		// if (instance.specifyHours) {
		// 	const isValid = Array.prototype.concat.apply([], days.map(d => [`${d}1`, `${d}2`]))
		// 		.some(d => this.organization[`${d}FromTime`] && this.organization[`${d}ToTime`]);
		// 	if (!isValid) {
		// 		return next('Since you have specifyHours selected, you just also select at least one set of valid times');
		// 	}
		// }

		// return next();
	});

	Organization.observe('after save', function(ctx) {
		if (ctx.isNewInstance) {
			if (ctx.options && ctx.options.accessToken) {
				return Organization.app.models.OrganizationUser.create({
					organizationId: ctx.instance.id,
					userId: ctx.options.accessToken.userId,
				}, ctx.options).then(orgUser => {
					console.log('user.afterRemote sending welcome email');

					const CustomUser = Organization.app.models.CustomUser;
					return CustomUser.findById(orgUser.userId).then(user => {
						console.log('got it', user);
						const baseUrl = Organization.app.WEB_URL;
						const text = `<html><body>
							<p>Thanks for signing up. Below is your login link.  Please bookmark this url:'</p>
							<p><a href="${baseUrl}/login">Login</a></p>
						</body></html>`;
						Organization.app.models.Email.send({
							to: user.email,
							from: 'Giveshare <info@giveshare.io>',
							subject: 'Welcome to GiveShare',
							html: text,
						}, function(err) {
							if (err) return console.log('> error sending welcome email', err);
							console.log('> sending welcome email to:', user.email);
						});

						return true;
					});
				});
			} else {
				console.log('nothing available', ctx);
			}
		} else {
			return Promise.resolve(true);
		}
	});

	Organization.getUniqueSlug = function(instance) {
		var slug = instance.slug;
		return Organization.findOne({where: {slug}}).then(org => {
			if (org) {
				var modifier = Math.floor(100 * Math.random());
				instance.slug = slug.concat(modifier);
				return Organization.getUniqueSlug(instance);
			} else {
				return true;
			}
		});
	};

	/**
	 * Gets publicly accessible data for an organization
	 * @param {Function(Error, )} callback
	 */

	Organization.prototype.getPublicData = function() {
		const publicData = Object.assign({}, this.__data);
		publicData.isActive = publicData.subscriptionRenewalDate > new Date();
		delete publicData.subscriptionRenewalDate;
		delete publicData.mgrOverRide;
		delete publicData.redemptionCode;
		delete publicData.defEmailContent;
		delete publicData.defEmailContentInvite;
		// TODO
		return Promise.resolve(publicData);
	};

	/**
	 * Get publicly accessible data for an organization by it's slug
	 * @param {string} slug The organization slug
	 */

	Organization.getPublicDataBySlug = function(slug, options) {
		//find the organization with the specified slug (case-insensitive)
		return Organization.findOne({where: {slug: {regexp: `/${slug}/i`}}}, options).then(org => {
			console.log('org', org);
			if (org) {
				return org.getPublicData();
			} else {
				return {};
			}
		});
	};

	/**
	 * Send the email response for the campaign
	 * @param {string} responseType The type of the email
	 * @param {number} campaignId The name of the campaign to respond to
	 * @param {number} optionId The id of the coupond option that was selected
	 * @param {object} options options from the request
	 */

	Organization.prototype.sendResponse = function(responseType, campaignId, optionId, options) {
		let status;
		// TODO
		return Organization.app.models.EmailTemplate
			.findOne({where: {organizationId: this.id, name: responseType}}, options)
			.then(template => {
				return Organization.app.models.Campaign.findById(campaignId, options)
					.then(campaign => [template, campaign]);
			})
			.then(([template, campaign]) => {
				if (optionId === 0) return [null, campaign, template];
				return Organization.app.models.OrganizationCouponOption.findById(optionId, options)
					.then(option => [option, campaign, template]);
			})
			.then(([option, campaign, template]) => {
				const baseHtml = '<p>' + template.message.split('\n')
					.join('</p><p>') + '</p>';
				let html = baseHtml.replace(/\{\{point-of-contact\}\}/, campaign.pointOfContactName)
					.replace(/\{\{charity-name\}\}/, campaign.charityName)
					.replace(/\{\{organization-name\}\}/, this.name)
					.replace(/\{\{organization-address\}\}/, `${this.address}\n${this.city}, ${this.state} ${this.zip}`)
					.replace(/\{\{organization-street-address\}\}/, this.address)
					.replace(/\{\{organization-city\}\}/, this.city)
					.replace(/\{\{organization-state\}\}/, this.state)
					.replace(/\{\{organization-zip\}\}/, this.zip)
					.replace(/\{\{campaign-url\}\}/, `${Organization.app.WEB_URL}/${this.slug}/${campaignId}`);

				if (option) {
					html = html.replace(
							/\{\{coupon-value\}\}/,
							(!option.discountAsPercent ? '$' : '') + option.discountAmount + (option.discountAsPercent ? '%' : '')
					);
				}

				return new Promise((res, rej) => {
					Organization.app.models.Email.send({
						to: campaign.pointOfContactEmail,
						from: 'Giveshare <info@giveshare.io>',
						subject: `Donation request ${responseType}`,
						html: html,
					}, function(err) {
						if (err) {
							console.log('> error sending approved campaign email', err);
							rej('error sending email');
						} else {
							res('success');
						}
					});
				});
			})
			.then(res => {
				return status;
			});
	};

	Organization.sendDailyEmailNew = function() {
		const dayCode = days[new Date().getDay()];

		const dayParam = `${dayCode}New`;
		const filter = {
			fields: ['id', 'name'],
			where: {},
			include: [
				{
					relation: 'organizationUsers',
					scope: {
						include: {
							relation: 'customUser',
							scope: {
								fields: ['firstName', 'lastName', 'email'],
								where: {
									recEmail: true,
								},
							},
						},
					},
				},
				{
					relation: 'campaigns',
					scope: {
						fields: ['id'],
						where: {
							status: 'requested',
						},
					},
				},
			],
		};
		filter.where[dayParam] = true;
		return Organization.find(filter).then(organizations => {
			console.log('orgs', organizations);
			return organizations.map(o => {
				return {
					id: o.id,
					name: o.name,
					users: o.organizationUsers().map(ou => {
						return ou.customUser();
					}).filter(u => u),
					campaigns: o.campaigns(),
				};
			});
		}).then(orgs => {
			orgs.forEach(o => {
				const emailBody = `Please find attached your email reminder for new campaign requests. 
				You currently have ${o.campaigns.length} new requests.
				Please click <a href='${Organization.app.WEB_URL}/Customized/${o.id}'>here</a> to address them.
				
				Have a great day!
				The team at GiveShare`;

				o.users.forEach(u => {
					return new Promise((res, rej) => {
						const perUserEmail = `Dear ${u.firstName},
							${emailBody}`.replace(/\n/g, '<br />');

						// console.log('email', {
						// 	to: u.email,
						// 	from: 'Giveshare <info@giveshare.io>',
						// 	subject: `New Giveshare Campaigns for ${o.name}`,
						// 	html: perUserEmail,
						// });
						Organization.app.models.Email.send({
							to: u.email,
							from: 'Giveshare <info@giveshare.io>',
							subject: `New Giveshare Campaigns for ${o.name}`,
							html: perUserEmail,
						}, function(err) {
							if (err) {
								console.log('> error sending new campaign email', err);
								rej('error sending email');
							} else {
								res('success');
							}
						});
					});
				});
			});

			return orgs;
		});
	};

	Organization.sendDailyEmailClosed = function() {
		const dayCode = days[new Date().getDay()];

		const dayParam = `${dayCode}Closed`;
		const filter = {
			fields: ['id', 'name'],
			where: {},
			include: [
				{
					relation: 'organizationUsers',
					scope: {
						include: {
							relation: 'customUser',
							scope: {
								fields: ['firstName', 'lastName', 'email'],
								where: {
									recEmail: true,
								},
							},
						},
					},
				},
				{
					relation: 'campaigns',
					scope: {
						fields: ['id'],
						where: {
							status: 'expired',
						},
					},
				},
			],
		};
		filter.where[dayParam] = true;
		return Organization.find(filter).then(organizations => {
			console.log('orgs', organizations);
			return organizations.map(o => {
				return {
					id: o.id,
					name: o.name,
					users: o.organizationUsers().map(ou => {
						return ou.customUser();
					}).filter(u => u),
					campaigns: o.campaigns(),
				};
			});
		}).then(orgs => {
			orgs.forEach(o => {
				const emailBody = `Please find attached your email reminder for expired campaigns. 
				You currently have ${o.campaigns.length} expired requests.
				Please click <a href='${Organization.app.WEB_URL}/Customized/${o.id}'>here</a> to address them.
				
				Have a great day!
				The team at GiveShare`;

				o.users.forEach(u => {
					return new Promise((res, rej) => {
						const perUserEmail = `Dear ${u.firstName},
							${emailBody}`.replace(/\n/g, '<br />');

						// console.log('email', {
						// 	to: u.email,
						// 	from: 'Giveshare <info@giveshare.io>',
						// 	subject: `New Giveshare Campaigns for ${o.name}`,
						// 	html: perUserEmail,
						// });
						Organization.app.models.Email.send({
							to: u.email,
							from: 'Giveshare <info@giveshare.io>',
							subject: `Expired Giveshare Campaigns for ${o.name}`,
							html: perUserEmail,
						}, function(err) {
							if (err) {
								console.log('> error sending new campaign email', err);
								rej('error sending email');
							} else {
								res('success');
							}
						});
					});
				});
			});

			return orgs;
		});
	};

	Organization.getOrganizationSummaries = function(filter) {
		console.log('filter', filter);
		const query = {
			fields: ['id', 'name', 'address', 'city', 'state', 'zip', 'website', 'phoneNumber'],
			include: {
				relation: 'organizationUsers',
				scope: {
					fields: ['userId'],
					limit: 1,
					include: {
						relation: 'customUser',
						scope: {
							fields: ['id', 'firstName', 'lastName', 'email'],
						},
					},
				},
			},
		};
		if (filter && filter.where) {
			query.where = filter.where;

		}

		return Organization.find(query).then(orgs => {
			return Promise.all(orgs.map(o => {
				return o.organizationSummary().then(summary => {
					o.user = o.organizationUsers().map(ou => ou.customUser())[0];
					o.unsetAttribute('organizationUsers');
					o.summary = summary;
					return o;
				});
			}));
		});
	};

	/**
	 * Summary of org restaurant value and community impact
	 */

	Organization.prototype.organizationSummary = function() {
		const summary = {};

		return Promise.all([
			this.campaigns.find({fields: ['id', 'discountPercentage', 'charityName']}).then(campaigns => {
            	summary.numCampaigns = campaigns.length;

				//charities
				const charityNames = campaigns.map(c => c.charityName);
				const uniqCharities = charityNames.filter((c, idx, arr) => arr.indexOf(c) === idx);
				summary.numCharities = uniqCharities.length;

				//time saved
				summary.timeSaved = campaigns.length * 15  / 60;

				return campaigns;
			}),
			this.campaignDetails.find({fields: ['CampaignId', 'TotalSales']}),
		]).then(([campaigns, campaignDetails]) => {
			summary.reach = campaignDetails.length;

			let total = 0;

			summary.totalSales = campaignDetails.reduce((agg, cur) => {
				return agg + cur.TotalSales;
			}, 0);

			campaignDetails.forEach(cd => {
				const campaign = campaigns.find(c => c.id.equals(cd.CampaignId)) || {discountPercentage: 0};
				if (!campaign || !campaign.discountPercentage) console.log('I lost', cd);
				cd.donation = cd.TotalSales * ((campaign.discountPercentage || 0) / 100);
			});

			summary.totalDonations = campaignDetails.reduce((agg, cur) => agg + cur.donation, 0);
		}).then(r => summary);
	};

	Organization.chargebeeWebhook = function(webhookBody) {
		// console.log('event', webhookBody.event_type && webhookBody.event_type === 'payment_succeeded');
		// console.log('subscription', webhookBody.content.subscription);
		// console.log('customer', webhookBody.content.customer && webhookBody.content.customer.cf_giveshare_customer_id);
		const content = webhookBody.content;
		if (webhookBody.event_type && webhookBody.event_type === 'payment_succeeded' &&
			content && content.subscription && content.customer && content.customer.cf_giveshare_customer_id
		) {
			const organizationId = content.customer.cf_giveshare_customer_id;
			return Organization.findById(organizationId).then(org => {
				if (!org) throw new Error('no organization found for "' + organizationId + '"');

				org.subscriptionRenewalDate = new Date(content.subscription.current_term_end * 1000);

				return org.save();
			});
		} else {
			return Promise.resolve(false);
		}
	};

	/**
	 * Export the emails from an organization's Campaign Details
	 * @param {string} campaignId An optional ID of a campaign to limit results to
	 */
// * @param {Function(Error, )} callback
	Organization.prototype.exportEmails = function(campaignId, cb) {
		const filter = {
			where: {OrgId: this.id},
			fields: ['Email'],
		};
		if (campaignId) {
			filter.where.CampaignId = campaignId;
		}

		Organization.app.models.CampaignDetails.find(filter).then(emails => {
			//console.log('emails', emails);

			const justEmails = emails.map(o => o.Email)
						.filter(e => e);
			const upperEmails = justEmails.map(e => e.toUpperCase());
			const uniques = justEmails.filter((e, idx) => upperEmails.indexOf(e.toUpperCase()) == idx)
						.map(e => ({Email: e}));
			//handle situation where there are no results yet
			if (!uniques.length) {
				uniques.push({Email: ''});
			}

			const XLSX = require('xlsx');
			const sheet = XLSX.utils.json_to_sheet(uniques);
			const wb = XLSX.utils.book_new();
			XLSX.utils.book_append_sheet(wb, sheet, 'Emails');

			//return cb(null, XLSX.write(wb, {type:'buffer', bookType:"xlsx"}), 'application/octet-stream');
			return cb(
				null,
				XLSX.write(wb, {type: 'buffer', bookType: 'xlsx'}),
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			);
		});
	};
};
