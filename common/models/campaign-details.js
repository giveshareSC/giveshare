module.exports = function(CampaignDetails) {
	CampaignDetails.prototype.getPublicData = function(includeOrg, options) {
		return new Promise((res, rej) => {
			if (!includeOrg) {
				res(null);
			} else {
				this.organization(options, (err, org) => {
					res(org.getPublicData());
				});
			}
		}).then(org => ({
			CampaignId: this.CampaignId,
			OrgId: this.OrgId,
			RequestDate: this.RequestDate,
			OpenDate: this.OpenDate,
			RedeemedDate: this.RedeemedDate,
			Status: this.Status,
			id: this.id,
			TotalSales: this.TotalSales,
			TotalGuests: this.TotalGuests,
			organization: org,
		}));
	};

	//add if to check empty redemption date
	CampaignDetails.observe('after save', function(ctx, next) {
		if (ctx.options && ctx.options.isMultiRedeem) {
			return next();
		} else if (ctx.instance && !ctx.instance.RedeemedDate) {
			sendEmail(ctx.instance).then(
				res => console.log('email res', res),
				err => console.log('email err', err)
			);
		}
		return next();
	});

	const sendEmail = function(details) {
		return new Promise((res, rej) => {
			details.organization((err, org) => {
				console.log('got org', err, org);

				details.campaign((cErr, campaign) => {
					const url = `${CampaignDetails.app.WEB_URL}/redeem/${details.id}`;
					console.log('passing url:', url);
					// const emailBody = `<p>${org.defOfferContent.split('\n').join('</p><p>')}</p>
					// 	<p>
					// 		Click here to redeem: <a href="${url}">redeem!</a>
					// 	</p>`;

					const emailBody = CampaignDetails.app.models.Campaign.replaceEmailPlaceholders(
						`<p style="font-size: 17px;">${org.defEmailContentInvite}</p><br>
						<p style="font-size: 12px; font-style: italic;">${org.offerTC}</p><br>
						<p style="font-size: 17px;">
							Click here to redeem: <a href="${url}">redeem!</a>
						</p>`,
						org,
						campaign
					);

					CampaignDetails.app.models.Email.send({
						to: details.Email,
						from: 'Giveshare <info@giveshare.io>',
						subject: `${org.name} will see you soon!`,
						html: emailBody,
					}, function(err) {
						if (err) {
							console.log('> error sending password reset email', err);
							rej('error sending email');
						} else {
							console.log('email sent');
							res('success');
						}
					});
				});
			});
		});
	};

	const isValidTime = function(details, organization) {
		if (!organization.specifyHours) return true;

		const now = new Date();
		//TODO fix this for server time
		//new Date('11/11/2017 00:00 EST')
		const day = now.getDay();

		const adjustedTime = new Date(now.toLocaleString('ISO', {timeZone: 'America/Detroit'}));
		const matime = adjustedTime.getHours().toString().padStart(2, '0') + ':' + adjustedTime.getMinutes().toString().padStart(2, '0');

		const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];

		const dayCode = days[day];
		console.log('checking ', days[day], ' times', matime, organization[`${dayCode}1FromTime`], organization[`${dayCode}1ToTime`], organization[`${dayCode}2FromTime`], organization[`${dayCode}2ToTime`]);
		return ((matime > organization[`${dayCode}1FromTime`] && matime < organization[`${dayCode}1ToTime`]) ||
			(matime > organization[`${dayCode}2FromTime`] && matime < organization[`${dayCode}2ToTime`]));

		/*
		let validTime = false;
		if (day === 0) {
			console.log('checking S times', this.organization.Sun1ToTime, organization.Sun2ToTime);
			if (matime > organization.Sun1FromTime && matime < organization.Sun1ToTime) {
				validTime = true;
			}
			if (matime > organization.Sun2FromTime && matime < organization.Sun2ToTime) {
				validTime = true;
			}
		}
		if (day === 1) {
			console.log('checking M times', this.organization.Mon1ToTime, organization.Mon2ToTime);
			if (matime > organization.Mon1FromTime && matime < organization.Mon1ToTime) {
				validTime = true;
			}
			if (matime > organization.Mon2FromTime && matime < organization.Mon2ToTime) {
				validTime = true;
			}
		}
		if (day === 2) {
			console.log('checking T times', this.organization.Tue1ToTime, organization.Tue2ToTime);
			if (matime > organization.Tue1FromTime && matime < organization.Tue1ToTime) {
				validTime = true;
			}
			if (matime > organization.Tue2FromTime && matime < organization.Tue2ToTime) {
				validTime = true;
			}
		}
		if (day === 3) {
			console.log('checking W times', this.organization.Wed1FromTime, organization.Wed1ToTime, matime, validTime);
			if (matime > organization.Wed1FromTime && matime < organization.Wed1ToTime) {
				validTime = true;
			}
			if (matime > organization.Wed2FromTime && matime < organization.Wed2ToTime) {
				validTime = true;
			}
		}
		if (day === 4) {
			console.log('checking TH times', this.organization.Thur1ToTime, organization.Thur1FromTime);
			if (matime > organization.Thur1FromTime && matime < organization.Thur1ToTime) {
				validTime = true;
			}
			if (matime > organization.Thur2FromTime && matime < organization.Thur2ToTime) {
				validTime = true;
			}
		}
		if (day === 5) {
			console.log('checking F times', this.organization.Fri1ToTime, organization.Fri2ToTime);
			if (matime > organization.Fri1FromTime && matime < organization.Fri1ToTime) {
				validTime = true;
			}
			if (matime > organization.Fri2FromTime && matime < organization.Fri2ToTime) {
				validTime = true;
			}
		}
		if (day === 6) {
			console.log('checking SA times', this.organization.Sat1ToTime, organization.Sat2ToTime);
			if (matime > organization.Sat1FromTime && matime < organization.Sat1ToTime) {
				validTime = true;
			}
			if (matime > organization.Sat2FromTime && matime < organization.Sat2ToTime) {
				validTime = true;
			}
		}

		return validTime;*/
	};

	CampaignDetails.prototype.redeem = function(value, numGuests, orgCode, options) {
		const Organization = CampaignDetails.app.models.Organization;
		const Campaign = CampaignDetails.app.models.Campaign;

		const childOrgFilter = {
			include: [{
				relation: "organizationUsers",
				scope: {
					include: {
						relation: 'customUser',
						scope: {
							fields: ["id", "email", "orgId"],
							include: {
								relation: "organizations",
								scope: {
									fields: ["id", "name", "redemptionCode", "mgrOverRide"],
									where: {
										"redemptionCode": orgCode,
										"subscriptionRenewalDate": {gt: new Date()},
									}
								}
							}
						}
					}
				}
			}]
		};
		return Organization.findById(this.OrgId, childOrgFilter, options).then(organization => {
			if (!organization) throw new Error('Organization not found');

			options.isMultiRedeem = this.RedeemedDate && organization.allowMultipleRedeems;

			if (this.RedeemedDate && !organization.allowMultipleRedeems) {
				// return {success: false, message: 'Already redeemed'};
				throw new Error('Already redeemed');
			}

			const orgUsers = organization.organizationUsers();
			const usrs = orgUsers && orgUsers.map(ou => ou.customUser());
			const relatedOrgs = usrs && usrs.filter(u => u).map(u => u.organizations());
			const childRedeemers = relatedOrgs && relatedOrgs.flat().filter(i => i);
			// const childRedCodes = [];
			// const childMgrOverRide = [];
			// childRedeemers.forEach(cr => {
			// 	childRedCodes.push(cr.redemptionCode);
			// 	childMgrOverRide.push(cr.mgrOverRide);
			// });

			if (orgCode === organization.redemptionCode) { // || childRedCodes.includes(orgCode)) {
				console.log('valid code - redeem!');

				if (value > organization.threshHold) {
					throw new Error('Could not redeem - amount requires manager approval');
				}
			} else if (orgCode === organization.mgrOverRide) { // || childMgrOverRide.includes(orgCode)) {
				console.log('mgr override - redeem!');
			} else if (childRedeemers && childRedeemers.length === 1) {
				this.RedeemingOrganizationId = childRedeemers[0].id;

				if (value > organization.threshHold) {
					throw new Error('Could not redeem - amount requires manager approval');
				}
			} else {
				throw new Error('Could not redeem - please check with an employee.');
			}

			if (!isValidTime(this, organization) && orgCode !== organization.mgrOverRide) { //validTime) {
				throw new Error('Time');
			}

			this.RedeemedDate = new Date();
			this.TotalGuests = numGuests;
			this.TotalSales = value;

			const filter = {
				where: {
					id: this.CampaignId,
				},
				include: [{
					relation: 'details',
					scope: {
						fields: ['id', 'CampaignId', 'TotalSales'],
					}
				}],
			};
			return Campaign.findOne(filter, options)
				.catch(err => {
					console.log('cant find campaign', this.CampaignId, err);
					throw new Error('Unable to find campaign');
				});
		})
		.then(camp => {
			//validate this is not expired
			if (camp.status !== 'active') {
				throw new Error('Could not redeem - Campaign is expired. Please see manager.');
			}

			// console.log('found campaign:', this.campaign);
			// console.log('current camp amt', this.campaign.resultingAmount);
			// console.log('details $$ added var:', this.details.TotalSales);
			// camp.resultingAmount = camp.resultingAmount + newDetails.TotalSales;
			const saveProm = options.isMultiRedeem ?
				CampaignDetails.create({
					CampaignId: this.CampaignId,
					Email: this.Email,
					OrgId: this.OrgId,
					RequestDate: this.RequestDate,
					OpenDate: new Date(),
					RedeemedDate: this.RedeemedDate,
					Status: 'redeemed',
					TotalSales: value,
					TotalGuests: numGuests
				}, options) :
				this.save(options);


			return saveProm.catch(err => {
				console.log('unable to redeem on campaign details', err);
				return new Error('Unable to redeem offer');
			}).then(dt => [camp, dt]);

		}).then(([camp, newDetails]) => {
			console.log('got new details', newDetails);
			//update the values for the item we just saved

			//pull the list of details records from the campaign
			const details = camp.details();
			// console.log('here are the retrieved detail records', details);

			//find the record that matches the newly saved value
			const matchDetails = details.find(d => d.id.toString() === newDetails.id.toString());
			if (matchDetails) {
				//if we found the match, then update it's properties
				// console.log('got match', matchDetails);
				Object.assign(matchDetails, newDetails);
				// console.log('finished match', matchDetails);
			} else {
				//we didn't already have this details record, so just add it for the following calc
				details.push(newDetails);
			}

			//recalculate the campaign amount
			console.log('here are the end detail records', details);
			camp.resultingAmount = details.reduce((agg, cur) => cur.TotalSales + agg, 0);

			return camp.save().then(res => ({
				success: true,
				message: 'Redeem successful.',
			})).catch(err => ({
				success: true,
				message: 'Unable to update campaign totals',
			}));
		})
		.catch(err => {
			console.log('CampaignDetails.redeem org retrieval error', err);
			return {success: false, message: err.message};//'Unable to retrieve organization'};
		});
	};
};

