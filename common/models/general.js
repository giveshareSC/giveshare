module.exports = function(General) {
	/**
	 * Sends a contact notification
	 * @param {string} contactName The name of the contact
	 * @param {string} contactEmail The email address of the contact
	 * @param {string} contactPhone The phone number of the contact
	 * @param {string} message The message that the user is trying to submit
	 * @param {string} captchaResponse The captchaResponse that is used to validate the request as being from a real person
	 * @param {Function(Error, boolean, string)} callback
	 */

	General.addContactRequest = function(contactName, contactEmail, contactPhone, message, captchaResponse, callback) {
		console.log('contactName', contactName);
		console.log('contactEmail', contactEmail);
		console.log('contactPhone', contactPhone);
		console.log('message', message);
		console.log('captchaResponse', captchaResponse);

		//TODO validate catcha item

		General.app.models.Email.send({
			to: 'steve@giveshare.io',
			from: 'Giveshare <info@giveshare.io>',
			subject: 'Contact form submitted',
			text: `Name: ${contactName}
					Email: ${contactEmail}
					Phone: ${contactPhone}
					Message: ${message}`.replace(/^\s*/mg, ''), //the /m flag enables multiline matching
		}, function(err) {
			if (err) {
				console.log('> error sending new campaign email', err);
				callback(null, false, 'error sending email');
			} else {
				callback(null, true, 'successfully send email');
			}
		});
	};
};
