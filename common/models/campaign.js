module.exports = function(Campaign) {
	Campaign.prototype.getPublicData = function(includeOrg, options) {
		return new Promise((res, rej) => {
			if (!includeOrg) {
				res(null);
			} else {
				this.organization(options, (err, org) => {
					res(org.getPublicData());
				});
			}
		}).then(org => ({
			charityName: this.charityName,
			description: this.description,
			discountPercentage: this.discountPercentage,
			startDate: this.startDate,
			status: this.status,
			endDate: this.endDate,
			id: this.id,
			mailingAddress: this.mailingAddress,
			mailingCity: this.mailingCity,
			mailingState: this.mailingState,
			mailingZip: this.mailingZip,
			organizationId: this.organizationId,
			organization: org,
		}));
	};

	Campaign.prototype.requestCoupon = function(email, options) {
		const filter = {
			where: {
				Email: email,
				CampaignId: this.id,
			},
		};
		console.log('should look for this campaign:', this.id);
		const CampaignDetails = Campaign.app.models.CampaignDetails;

		return Promise.all([
			CampaignDetails.find(filter),
			new Promise((res, rej) => this.organization((err, org) => {
				if (err) return rej(err);
				res(org);
			}))
		]).then(([deets, org]) => {
			console.log('number of records found!', deets.length);
			console.log('filter!', filter);

			if (deets.length === 0 || org.allowMultipleRedeems) {
				const newDetails = {
					Email: email,
					RequestDate: new Date(),
					RedeemedDate: null,
					OrgId: this.organizationId,
					TotalSales: 0,
					CampaignId: this.id,
				};
				return CampaignDetails.create(newDetails).then(saveRes => ({
					message: 'Invite has been emailed to you!!',
					success: true,
				})).catch(err => {
					console.log('Error creating campaign details', err);
					return {
						message: 'An error occurred while requesting this coupon. Please try again later.',
						success: false,
					};
				});
			} else {
				return {
					message: 'This email has already been used for this coupon, please use a different email account!',
					success: false,
				};
			}
		});
	};

	Campaign.observe('before save', function(ctx) {
		console.log('about to save campaign', ctx.instance);

		//if this is a new instance, then set all of the properties
		if (ctx.isNewInstance) {
			const inst = ctx.instance;

			//force props to have default values
			inst.discountPercentage = null;
			inst.resultingAmount = 0;
			inst.isRead = false;

			return Campaign.app.models.Organization.findById(inst.organizationId).then(org => {
				if (org.allowCustom) {
					inst.status = 'requested';
				} else {
					inst.status = 'active';
					inst.discountPercentage = org.discountAmount;

					const today = new Date();
					const endDate = new Date();
					endDate.setDate(endDate.getDate() + org.daysValid);
					endDate.setHours(23, 59, 59, 999);

					inst.startDate = today;
					inst.reqDate = today;
					inst.endDate = endDate;
				}

				return true;
			});
		} else {
			return Promise.resolve(true);
		}
	});
	Campaign.observe('after save', function(ctx, next) {
		if (ctx.instance) {
			if (ctx.instance.status === 'active' && !ctx.instance.urlLink) {
				//need to send email

				ctx.instance.urlLink = `${Campaign.app.WEB_URL}/campaigns/${ctx.instance.id}`;
				ctx.instance.save();
				sendEmail(ctx.instance)
					.then(res => console.log('email res', res),
							err => console.log('email err', err)
					);
			}
			return next();
		} else {
			next();
		}
	});

	Campaign.closeExpired = function(callback) {
		const midnight = new Date();
		midnight.setHours(0, 0, 0, 0);

		Campaign.updateAll(
			{
				endDate: {lt: midnight},
				status: 'active',
			},
			{
				status: 'expired',
			},
			(err, res) => {
				console.log('result from closeExpired', err, res);
				callback(err, res);
			}
		);
	};

	Campaign.replaceEmailPlaceholders = function(emailBody, org, campaign) {
		return emailBody.replace(/\{\{point-of-contact\}\}/g, campaign.pointOfContactName)
			.replace(/\{\{charity-name\}\}/g, campaign.charityName)
			.replace(/\{\{campaign-end\}\}/g, campaign.endDate ? campaign.endDate.toLocaleDateString('en-US') : '')
			.replace(/\{\{organization-name\}\}/g, org.name)
			.replace(/\{\{organization-address\}\}/g, `${org.address}<br />${org.city}, ${org.state} ${org.zip}`)
			.replace(/\{\{organization-street-address\}\}/g, org.address)
			.replace(/\{\{organization-city\}\}/g, org.city)
			.replace(/\{\{discount-percent\}\}/g, campaign.discountPercentage)
			.replace(/\{\{organization-state\}\}/g, org.state)
			.replace(/\{\{organization-zip\}\}/g, org.zip);
	};

	const sendEmail = function(campaign) {
		return new Promise((res, rej) => {
			campaign.organization((err, org) => {
				console.log('got org', err, org);

				const emailBody = `<p>${org.defEmailContent.split('\n').join('</p><p>')}</p><br>
					<p>
						Please share this url with your community: <a href="${campaign.urlLink}">${campaign.urlLink}</a>
					</p>`;

				Campaign.app.models.Email.send({
					to: campaign.pointOfContactEmail,
					from: 'Giveshare <info@giveshare.io>',
					subject: `${org.name} is happy to help`,
					html: Campaign.replaceEmailPlaceholders(emailBody, org, campaign),
				}, function(err) {
					if (err) {
						console.log('> error sending campaign approval email', err);
						rej('error sending email');
					} else {
						console.log('email sent');
						res('success');
					}
				});
			});
		});
	};
	// Campaign.expire = function() {
	// 	const yesterday = [new Date().getDate() - 1];
	// 	const filter = {
	// 		where: {
	// 			status: 'active',
	// 			endDate: yesterday,
	// 		},
	// 	};
	// 	return Campaign.find(filter).then(camps => {
	// 		camps.forEach(
	// 			return new Promise((res, rej) => {
    //
	// 			})
	// 		)
	// 		});
	// };

	Campaign.sendCampaignExpirationReminder = function() {
		let expirationDateNoticeStart = new Date();
		expirationDateNoticeStart.setDate(expirationDateNoticeStart.getDate() + 5);
		expirationDateNoticeStart.setUTCHours(0, 0, 0, 0);

		let expirationDateNoticeEnd = new Date();
		expirationDateNoticeEnd.setDate(expirationDateNoticeEnd.getDate() + 5);
		expirationDateNoticeEnd.setUTCHours(23, 59, 59, 999);

		console.log('Expiration Date span: ', expirationDateNoticeStart.toISOString(), expirationDateNoticeEnd.toISOString());

		const filter = {
			fields: ['id', 'organizationId', 'charityName', 'endDate', 'status'],
			where: {
				endDate: {between: [expirationDateNoticeStart.toISOString(), expirationDateNoticeEnd.toISOString()]},
				status: 'active',
			},
			include: [
				{
					relation: 'organization',
					scope: {
						fields: ['id', 'name', 'slug', 'offerTC'],
					},
				},
				{
					relation: 'details',
					scope: {
						fields: ['id', 'Email', 'RedeemedDate'],
						where: {
							RedeemedDate: null,
						},
					},
				},
			],
		};
		//console.log('filter', JSON.stringify(filter, null, 2));
		return Campaign.find(filter).then(campaigns => {
			return campaigns.map(c => {
				return {
					id: c.id,
					charityName: c.charityName,
					endDate: c.endDate,
					orgName: c.organization().name,
					tcs: c.organization().offerTC,
					details: c.details(),
				};
			});
		}).then(campaigns => {
			campaigns.forEach(c => {
				console.log('\n\nCampaign: ', c);

				c.details.forEach(d => {
					const emailBody = `<p style="font-size: 17px;">Hello from GiveShare!</p>
					<p style="font-size: 17px;">Just a friendly reminder that ${c.charityName}'s campaign at ${c.orgName} expires on ${c.endDate.toLocaleDateString('en-us')}.</p>
					<p style="font-size: 17px;">Thanks for supporting your community!</p>
					<p style="font-size: 17px;">Cheers, <br> The GiveShare Team</p>
					<p style='font-size: 12px; font-style: italic'>${c.tcs}</p>
					<p style="font-size: 17px;">Click here to <a href='${Campaign.app.WEB_URL}/redeem/${d.id}'>redeem</a>.</p>`;

					console.log(emailBody);

					return new Promise((res, rej) => {
						const perUserEmail = emailBody.replace(/\t/g, '');

						//console.log('\n\nEmail: ', {
						//	to: d.Email,
						//	from: 'Giveshare <steve@giveshare.io>',
						//	subject: `${c.charityName}'s campaign at ${c.orgName} expires on ${c.endDate}`,
						//	html: perUserEmail
						//});

						Campaign.app.models.Email.send({
							to: d.Email,
							from: 'Giveshare <steve@giveshare.io>',
							subject: `${c.charityName}'s campaign at ${c.orgName} expires on ${c.endDate.toLocaleDateString('en-us')}`,
							html: perUserEmail,
						}, function(err) {
							if (err) {
								console.log('> error sending campaign expiration reminder email', err);
								rej('error sending email');
							} else {
								res('success');
							}
						});
					});
				});
			});

			return campaigns;
		});
	};
};
