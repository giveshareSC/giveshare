# Giveshare

Angular / Loopback project. 

Uses Mongodb as a backend.

The Node server is run via `forever` package.

To restart the project on the server, you need to run `./restart.sh` to set the `GIVESHARE_WEB_URL`, `NODE_ENV`, `HTTPS`, `TLS_CERT_KEY`, and `TLS_CERT` variables. 

In order to run without escalation on the server, project listens on port 8000 and 3000 and has HTTPS redirection from port 8000 to 3000.

The server uses `iptables` to redirect `8000 -> 80` and `3000 -> 443`. `iptables` rules are:
```
*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [20:1200]
:OUTPUT ACCEPT [14:1145]
:POSTROUTING ACCEPT [14:1145]
-A PREROUTING -i eth0 -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 8080
-A PREROUTING -i eth0 -p tcp -m tcp --dport 443 -j REDIRECT --to-ports 3000
COMMIT
```

These rules are run on the server at reboot by this line being added to script in `/etc/rc.local`:
```
/sbin/iptables-restore /home/ec2-user/iptables.values
```