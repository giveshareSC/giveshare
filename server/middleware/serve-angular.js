module.exports = function() {
	return function tracker(req, res, next) {
		//if this is a get request to a non-core-LoopBack URL, pass it off to the Angular application
		if (req.method === 'GET' && !req.path.startsWith('/api') && req.path != '/explorer') {
			res.sendFile('client/dist/index.html', {root: '.'});
		} else {
			//if this is not a GET or is to a core LoopBack URL, just pass this along
			next();
		}
	};
}
