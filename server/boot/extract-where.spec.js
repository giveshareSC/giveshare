if (global.jasmine) {
	describe('extract-where.js', () => {
		const extractor = require('./extract-where');

		it('should extract simple filter (1)', (() => {
			let filter = {
				id: 1,
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [1]});
		}));

		it('should extract simple filter (2)', (() => {
			let filter = {
				id: {inq: [1, 5]},
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [1, 5]});
		}));

		it('should extract and', (() => {
			let filter = {
				and: [
					{
						id: {inq: [1, 5]},
					}, {
						communityId: 12,
					},
				],
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [1, 5], communityId: [12]});
		}));

		it('should extract or', (() => {
			let filter = {
				or: [
					{
						id: {inq: [1, 5]},
					}, {
						communityId: 12,
					},
				],
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [1, 5], communityId: [12]});
		}));

		it('should extract both ands and ors (1)', (() => {
			let filter = {
				or: [
					{id: {eq: 1}},
					{id: 2},
					{communityId: 12},
					{
						and: [
							{face: 2},
							{thing: 4},
							{id: 42},
						],
					},
				],
				and: [
					{id: {nin: [2324, 23]}},
					{id: {inq: [6, 5]}},
					{me: 'yes'},
					{you: 'no'},
				],
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [1, 2, 6, 5, 42], communityId: [12]});
		}));

		it('should extract both ands and ors (2)', (() => {
			let filter = {
				and: [
					{id: {nin: [2324, 23]}},
					{id: {inq: [6, 5]}},
					{me: 'yes'},
					{you: 'no'},
				],
				or: [
					{id: {eq: 1}},
					{id: 2},
					{communityId: 12},
					{
						and: [
							{face: 2},
							{thing: 4},
							{id: 42},
						],
					},
				],
			};

			let relationIds = ['id', 'communityId'];

			const res = extractor.getRelationValuesInWhere(filter, relationIds);
			expect(res).toEqual({id: [6, 5, 1, 2, 42], communityId: [12]});
		}));
	});
}
