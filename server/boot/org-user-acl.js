module.exports = function(app) {
	const extractor = require('./extract-where');
	const Role = app.models.Role;

	const userFilter = {
		include: 'organizationUsers',
	};

	// function matches(id1, id2) {
	// 	if (id1 === undefined || id1 === null || id1 === '' ||
	// 		id2 === undefined || id2 === null || id2 === '') {
	// 		return false;
	// 	}
	// 	// The id can be a MongoDB ObjectID
	// 	return id1 === id2 || id1.toString() === id2.toString();
	// }
	function getWhereClause(req) {
		if (!req.query || (!req.query.where && !req.query.filter)) return {};
		const where = req.query.filter || req.query.where;

		try {
			if (typeof where === 'string') return JSON.parse(where);
		} catch (err) {
			console.log('coordinator acl error parsing where clause json', where, err);
			throw err;
		}
		return where;
	}

	//need relationFields -> id or [id]
	//need to parse ors, and ands
	//need to handle inq
	//meed to handle eq
	//DO NOT need to handle ninq/nin
	//DO NOT need to handle neq
	//reject gt, gte, lt, lte, like, between, near, ilike, nilike, regexp
	function parseWhereClause(where, allRelationKeys) {
		return extractor.getRelationValuesInWhere(where, allRelationKeys);
	}

	function isClass(modelClass, correctClassName) {
		if (!modelClass) return false;
		const correctClass = modelClass.modelBuilder.models[correctClassName];
		if (!correctClass) return false;
		return modelClass === correctClass || modelClass.prototype instanceof correctClass;
	}

	function getUserWithRelationData(app, userId) {
		if (!userId) return Promise.reject(new Error('no user id'));

		return app.models.CustomUser.findById(userId, userFilter).then(user => {
			//get the ids of schools and communities of which this user is a coord
			const organizationIds = user.organizationUsers().map(su => su.organizationId.toString());

			return {user, organizationIds};
		});
	}

	function getModelData(context, orgRelIds) {
		if (!context.modelId) {
			try {
				if (!context.remotingContext || !context.remotingContext.req)
					return Promise.resolve({});

				const whereBody = parseWhereClause(
					getWhereClause(context.remotingContext.req),
					orgRelIds
				);

				switch (context.remotingContext.method.accessType) {
					case 'WRITE' :
						let modelData = context.remotingContext.req.body;
						if (!modelData) modelData = whereBody;
						return Promise.resolve(modelData);
					case 'READ':
						return Promise.resolve(whereBody);
					default:
						return Promise.resolve({});
				}
			} catch (ex) {
				console.log('error while parsing where', ex);
				return Promise.resolve({});
			}
		}
		return context.model.findById(context.modelId);
	}

	const isOrganizationUser = function(role, context, cb) {
		console.log('role', role);
		//Q: Is the user logged in? (there will be an accessToken with an ID if so)
		const userId = context.accessToken && context.accessToken.userId;
		if (!userId) {
			// console.log('no user');
			//A: No, user is NOT logged in: callback with FALSE
			return process.nextTick(() => cb(null, false));
		}

		//when user is orgUser: return cb(null, true);
		//when user is !orgUser: return cb(null, false);
		getUserWithRelationData(app, userId).then(res => {
			// console.log('orgUser role resolver',

			//Step 1: is this user a coordinator?
			//this user it not a coordinator. Just stop...
			if (!res.organizationIds.length) return cb(null, false);

			//console.log('context', context);
			//if this is an existing model
			if (context.modelId) {
				//Step 2: if this class is directly Organization, just check the IDs
				if (isClass(context.model, 'Organization')) {
					console.log('checking Org', res.organizationIds.includes(context.modelId));
					return cb(null, res.organizationIds.includes(context.modelId.toString()));
				}
				//If this class is the user class, see if the user belongs to the current users org
				if (isClass(context.model, 'CustomUser')) {
					const userFilter = {where: {userId: context.modelId}};
					context.model.modelBuilder.models['OrganizationUser'].find(userFilter).then(orgUsers => {
						return cb(null, orgUsers.some(su => res.organizationIds.includes(su.organizationlId)));
					});

					return;
				}
			}

			const belongsTo = Object.keys(context.model.relations)
				.map(rName => context.model.relations[rName])
				.filter(r => 'belongsTo' === r.type);
			const orgRelIds = belongsTo
				.filter(r => isClass(r.modelTo, 'Organization'))
				.map(r => r.keyFrom);

			//if the object has no relations, bail
			if (!orgRelIds.length) {
				console.log('no org rels');
				return cb(null, false);
			}

			// Step 3: if the user is a orgUser of something, lookup the requested model
			return getModelData(context, orgRelIds).then(model => {
				// A: There's no model by this ID! Pass error to callback
				if (!model) {
					// console.log('no object found');
					return cb(new Error('Unable to find object'));
				}

				let hasValidIds = false;
				let hasInvalidIds = false;

				console.log('about to check', orgRelIds, res.organizationIds);

				//handle when the orgUser is referenced directly
				function compareAllIds(columnNames, itemIds) {
					columnNames.forEach(id => {
						if (typeof model[id] !== 'undefined') {
							const modelId = model[id].toString();
							const isArray = Array.isArray(modelId);
							if (!isArray && itemIds.includes(modelId)) {
								//the user is a coordinator of the accessed entity
								hasValidIds = true;
							} else if (isArray && model[id].every(i => itemIds.includes(i))) {
								hasValidIds = true;
							} else {
								hasInvalidIds = true;
							}
						}
					});
				}

				//check that this user is a coordinator of all of the accessed entities
				compareAllIds(orgRelIds, res.organizationIds);

				console.log('Checking final', hasValidIds && !hasInvalidIds);
				return cb(null, hasValidIds && !hasInvalidIds);
			});
		})
			.catch(err => cb(err)); // A: The datastore produced an error! Pass error to callback
	};

	Role.registerResolver('organizationUser', isOrganizationUser);
	console.log('registered orgUser acl');

	return isOrganizationUser;
};
