if (global.jasmine) {
	describe('Coordinator ACL', () => {
		const extractor = require('./extract-where');

		const app = {
			models: {
				CustomUser: {
					findById: null,
				},
				Role: {
					registerResolver: jasmine.createSpy('registerResolver'),
				},
			},
		};
		let mockContext = {};
		const getBasicUserReturn = function(orgIds) {
			const organizationsUsers = orgIds.map(id => ({organizationId: id}));
			return Promise.resolve({
				id: 42,
				username: 'testerton',
				organizationUsers: () => organizationsUsers,
			});
		};

		const isOrgUser = require('./org-user-acl')(app);
		const GenericClass = function(modelClasses) {
			this.modelBuilder = {models: modelClasses};
			this.relations = {};
			this.findById = jasmine.createSpy('model.findById');
		};
		const Organization = function(modelClasses) {
			this.modelBuilder = {models: modelClasses};
			this.relations = {};
			this.findById = jasmine.createSpy('model.findById');
		};
		const modelClasses = {Organization};

		beforeAll(() => {
			// spyOn(extractor, 'getRelationValuesInWhere').and.returnValue({schoolId: 1});
		});

		beforeEach(() => {
			spyOn(extractor, 'getRelationValuesInWhere').and.returnValue({schoolId: 1});
			//extractor.getRelationValuesInWhere.calls.reset();
			app.models.CustomUser.findById = jasmine.createSpy('CustomUser.findById');
			mockContext = {
				accessToken: {userId: 42},
				model: new GenericClass(modelClasses),
				remotingContext: {
					method: {accessType: 'WRITE'},
					req: {},
				},
			};

			// why?? In order to get class check to pass, I have to reset the prototype object
			mockContext.model.prototype = new GenericClass();

			//set default relations
			mockContext.model.relations = {
				organization: {
					modelTo: new Organization(modelClasses),
					type: 'belongsTo',
					keyFrom: 'organizationId',
				},
			};
			mockContext.model.relations.organization.modelTo.prototype = new Organization();
		});

		it('should reject if no userId', ((done) => {
			const uId = 1;
			isOrgUser('test', {}, (err, response) => {
				expect(app.models.CustomUser.findById).not.toHaveBeenCalled();
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		}));

		it('should lookup user if userId', ((done) => {
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([], [999]));
			mockContext.model.findById.and.returnValue(Promise.resolve({}));

			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		}));

		it('should lookup model if modelId', ((done) => {
			mockContext.modelId = 6;
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([999]));
			//return nothing to cause fail
			mockContext.model.findById.and.returnValue(Promise.resolve({}));

			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);
				expect(mockContext.model.findById).toHaveBeenCalledWith(mockContext.modelId);
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		}));

		it('should reject if user has no organization', ((done) => {
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([]));
			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		}));

		it('should reject if accessing same class (Organization) but wrong model', ((done) => {
			//copy old values to replace
			mockContext.model = new Organization(modelClasses);
			mockContext.model.prototype = new Organization();

			mockContext.modelId = 42;
			app.models.CustomUser.findById.and.returnValue(Promise.resolve({
				organizationUsers: () => [
					{
						organizationId: 99,
					},
				],
			}));
			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);

				expect(mockContext.model.findById).not.toHaveBeenCalled();
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		}));

		it('should pass if accessing same class (Organization)', ((done) => {
			//copy old values to replace
			mockContext.model = new Organization(modelClasses);
			mockContext.model.prototype = new Organization();

			mockContext.modelId = 42;
			app.models.CustomUser.findById.and.returnValue(Promise.resolve({
				organizationUsers: () => [
					{
						organizationId: 42,
					},
				],
			}));
			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);

				expect(mockContext.model.findById).not.toHaveBeenCalled();
				expect(err).toBeFalsy();
				expect(response).toBeTruthy();
				done();
			});
		}));

		it('should lookup model if modelId and pass if only relation matches', ((done) => {
			mockContext.modelId = 6;

			const orgIdToPass = 12;
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([orgIdToPass]));
			mockContext.model.findById.and.returnValue(Promise.resolve({organizationId: orgIdToPass}));

			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalledWith(
					mockContext.accessToken.userId,
					jasmine.any(Object)
				);
				expect(mockContext.model.findById).toHaveBeenCalledWith(mockContext.modelId);
				expect(err).toBeFalsy();
				expect(response).toBeTruthy();
				done();
			});
		}));

		it('should pull body if WRITE and no modelId', (done) => {
			const orgIdToPass = 21;
			app.models.CustomUser.findById.and.returnValue(
				getBasicUserReturn([orgIdToPass])
			);

			mockContext.model.relations.organization.keyFrom = 'face';

			mockContext.remotingContext.req.body = {face: orgIdToPass};
			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalled();
				expect(mockContext.model.findById).not.toHaveBeenCalled();
				expect(extractor.getRelationValuesInWhere).toHaveBeenCalledWith({}, ['face']);
				expect(err).toBeFalsy();
				expect(response).toBeTruthy();
				done();
			});
		});

		it('should call extractor (where) if WRITE, no modelId or body (fail - multi)', (done) => {
			const orgIdToPass = 21;
			app.models.CustomUser.findById.and.returnValue(
				getBasicUserReturn([orgIdToPass])
			);

			const where = {garbage: 1};
			mockContext.remotingContext.req.query = {where: where};
			extractor.getRelationValuesInWhere.and.returnValue({orgId: [1, orgIdToPass]});

			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalled();
				expect(mockContext.model.findById).not.toHaveBeenCalled();
				expect(extractor.getRelationValuesInWhere).toHaveBeenCalledWith(where, ['organizationId']);
				expect(err).toBeFalsy();
				expect(response).toBeFalsy();
				done();
			});
		});

		it('should call extractor (where) if WRITE, no modelId or body (pass - only)', (done) => {
			const orgIdToPass = 21;
			app.models.CustomUser.findById.and.returnValue(
				getBasicUserReturn([orgIdToPass])
			);

			const where = {garbage: 1};
			mockContext.remotingContext.req.query = {where: where};
			mockContext.model.relations.organization.keyFrom = 'field';

			extractor.getRelationValuesInWhere.and.returnValue({field: [orgIdToPass]});

			isOrgUser('test', mockContext, (err, response) => {
				expect(app.models.CustomUser.findById).toHaveBeenCalled();
				expect(mockContext.model.findById).not.toHaveBeenCalled();
				expect(extractor.getRelationValuesInWhere).toHaveBeenCalledWith(where, ['field']);
				expect(err).toBeFalsy();
				expect(response).toBeTruthy();
				done();
			});
		});

		it('should call extractor (where) if READ, no modelId', (done) => {
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([21]));

			const where = {garbage: 1};
			mockContext.remotingContext.method.accessType = 'READ';
			mockContext.remotingContext.req.query = {where: where};

			isOrgUser('test', mockContext, (err, response) => {
				expect(extractor.getRelationValuesInWhere).toHaveBeenCalledWith(where, ['organizationId']);
				expect(err).toBeFalsy();
				done();
			});
		});

		it('should call extractor (filter) if READ, no modelId', (done) => {
			app.models.CustomUser.findById.and.returnValue(getBasicUserReturn([21]));

			const where = {garbage: 1};
			mockContext.remotingContext.method.accessType = 'READ';
			mockContext.remotingContext.req.query = {filter: where};

			isOrgUser('test', mockContext, (err, response) => {
				expect(extractor.getRelationValuesInWhere).toHaveBeenCalledWith(where, ['organizationId']);
				expect(err).toBeFalsy();
				done();
			});
		});
	});
}
