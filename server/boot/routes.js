// Copyright IBM Corp. 2014,2015. All Rights Reserved.
// Node module: loopback-example-user-management
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

module.exports = function(app) {
	const User = app.models.CustomUser;
	const AffiliateToken = app.models.AffiliateToken;

	//verified
	app.get('/verified', function(req, res) {
		res.render('verified', {});
	});

	//log a user out
	app.get('/logout', function(req, res, next) {
		if (!req.accessToken) return res.sendStatus(401);
		User.logout(req.accessToken.id, function(err) {
			if (err) return next(err);
			res.redirect('/');
		});
	});

	//send an email with instructions to reset an existing user's password
	app.post('/request-password-reset', function(req, res, next) {
		User.resetPassword({
			email: req.body.email,
		}, function(err) {
			if (err) return res.status(401).send(err);

			res.render('response', {
				title: 'Password reset requested',
				content: 'Check your email for further instructions',
				redirectTo: '/',
				redirectToLinkText: 'Log in',
			});
		});
	});

	//show password reset form
	app.get('/reset-password', function(req, res, next) {
		if (!req.accessToken) return res.sendStatus(401);
		res.render('password-reset', {
			accessToken: req.accessToken.id,
		});
	});

	//reset the user's pasword
	app.post('/reset-password', function(req, res, next) {
		if (!req.accessToken) return res.sendStatus(401);

		//verify passwords match
		if (!req.body.password ||
			!req.body.confirmation ||
			req.body.password !== req.body.confirmation
		) {
			return res.sendStatus(400, new Error('Passwords do not match'));
		}

		User.findById(req.accessToken.userId, function(err, user) {
			if (err) return res.sendStatus(404);
			user.updateAttribute('password', req.body.password, (err, user) => {
				if (err) return res.sendStatus(404);
				console.log('> password reset processed successfully');
				res.render('response', {
					title: 'Password reset success',
					content: 'Your password has been reset successfully',
					redirectTo: '/',
					redirectToLinkText: 'Log in',
				});
			});
		});
	});

	// Add custom referral route
	app.get('/affiliate/:affiliateKey', function(req, res, next) {
		// console.log('req', req);
		const referrer = req.get('Referrer') || '';
		const affiliate = req.params.affiliateKey || '';

		console.log('affiliate', referrer, affiliate);
		// console.log('models', app.models.AffiliateToken);
		// AffiliateToken.find().then(r => console.log('tokens', r)).catch(e => console.log('token errors', e));

		let passes = false;
		switch (affiliate.toUpperCase()) {
			case 'DMS':
				passes = (/https?:\/\/neato\.samcart\.com\/referral/.test(referrer));
				break;
			//add any other affiliates here
		}

		//check if the confirm token and the referrer match
		if (passes) {
			const tokenData = {
				affiliate,
				source: referrer,
				clientIP: (req.headers['x-forwarded-for'] || req.connection.remoteAddress || '').split(',')[0].trim(),
			};

			console.log('new token', tokenData);
			AffiliateToken.create(tokenData).then(newToken => {
				// console.log('new token created', newToken);
				//set an affiliate token (valid for 2 hours)
				res.cookie('affiliate_token', newToken.id.toString(), {maxAge: 1000 * 60 * 60 * 2});
				//Redirect the user to the create page. From there, they will be use the token to bypass the billing process
				res.redirect(`${app.WEB_URL}/create`);
			}).catch(err => {
				console.log('affiliate token creation error', err);
				res.status(404).send('Page not found.');
			});
		} else {
			res.status(404).send('Page not found');
		}
	});

};
