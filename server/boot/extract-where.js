
const keysToParse = ['and', 'or'];
const keysToExtract = ['eq', 'inq'];
const keysToIgnore = ['neq', 'ninq', 'nin'];
const keysToError =
	['gt', 'gte', 'lt', 'lte', 'like', 'between', 'near', 'ilike', 'nilike', 'regexp'];

function removeOrsAndAnds(obj, finalObj) {
	obj = flattenClauseArray(obj);

	//if (typeof obj === "object") return obj
	return obj.reduce((agg, cur) => {
		//console.log(agg, cur)
		Object.keys(cur).forEach(k => {
			//console.log('agg', agg, k);
			if (agg[k]) {
				//console.log('agg.push', agg[k])
				agg[k].push(cur[k]);
			} else {
				agg[k] = [cur[k]];
			}
		});
		return agg;
	}, finalObj);
}
function flattenClauseArray(objToFlatten) {
	if (!Array.isArray(objToFlatten) || !objToFlatten.length || !Array.isArray(objToFlatten[0])) {
		return objToFlatten;
	}

	return flattenClauseArray(Array.prototype.concat.apply([], objToFlatten));
}

function extractFieldsFromClauses(obj) {
	let nonIssueFields;
	do  {
		//find all fields that aren't and/or and add them to a new obj
		nonIssueFields = Object.keys(obj)
								.filter(k => !keysToParse.includes(k))
								.reduce((agg, k) => {
									//console.log('my Obj', obj);
									agg[k] = obj[k];
									return agg;
								}, {});

		//loop through keys on the object that need to be replaced
		Object.keys(obj)
			.filter(k => keysToParse.includes(k))
			.forEach(key => {
				//remove call the funciton to extract the and/or content
				//add this content to the "good" fields
				removeOrsAndAnds(obj[key], nonIssueFields);
			});

		//replace our current object with the good object
		obj = nonIssueFields;

		//as long as some of the keys in the obj need to be replaced, repeat
	} while (!Object.keys(obj).every(k => !keysToParse.includes(k)));

	//return our extracted object (all props should be on the top level)
	return obj;
}

function getValuesForRelations(whereClause, relationIds) {
	let parsedObj = extractFieldsFromClauses(whereClause);
	return Object.keys(parsedObj)
		.filter(k => relationIds.includes(k))
		.map(k => {
			return {
				key: k,
				//value: !Array.isArray(parsedObj[k]) ? parsedObj[k] : parsedObj[k].map(elem => {
				value: (!Array.isArray(parsedObj[k]) ? [parsedObj[k]] : parsedObj[k]).map(elem => {
					if (typeof elem == 'object') {
						let objKey = Object.keys(elem)[0];
						if (keysToExtract.includes(objKey)) return elem[objKey];
						if (keysToIgnore.includes(objKey)) return undefined;
						if (keysToError.includes(objKey))
							throw new Error('Where used illegal key ' + objKey);
					}
					return elem;
				})
				.filter(i => i)
				.reduce((agg, cur) => {
					if (Array.isArray(cur)) {
						Array.prototype.push.apply(agg, cur);
					} else {
						agg.push(cur);
					}
					return agg;
				}, []),
			};
		})
		.reduce((accumulator, cur) => {
			accumulator[cur.key] = cur.value;
			return accumulator;
		}, {});
}
//
// let filter = {
// 	or: [
// 		{id: {eq: 1}},
// 		{id: 2},
// 		{communityId: 12},
// 		{
// 			and: [
// 				{face: 2},
// 				{thing: 4},
// 				{id: 42},
// 			],
// 		},
// 	],
// 	and: [
// 		{id: {nin: [2324, 23]}},
// 		{id: {inq: [6, 5]}},
// 		{me: 'yes'},
// 		{you: 'no'},
// 	],
// };
//
// let relationIds = ['id', 'communityId'];
//
// console.log('rel-obj', getValuesForRelations(filter, relationIds));

module.exports = {
	getRelationValuesInWhere: getValuesForRelations,
};
