const loopback = require('loopback');
const boot = require('loopback-boot');
const path = require('path');

const fs = require('fs');

const http = require('http');
const https = require('https');

const app = module.exports = loopback();

const sslConfig = require('ssl-config')('modern');

// configure view handler
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.WEB_URL = process.env.GIVESHARE_WEB_URL || 'http://localhost:4200';

app.start = function() {
	const isHttps = process.env.HTTPS;
	console.log('HTTPS Environmental Variable: ', process.env.HTTPS);
	console.log('NODE_ENV: ', process.env.NODE_ENV);

	let server = null;
	const constants = require('constants');
	if (isHttps) {
		const options = {
			key: fs.readFileSync(process.env.TLS_CERT_KEY),
			cert: fs.readFileSync(process.env.TLS_CERT),
			ciphers: sslConfig.ciphers,
			honorCipherOrder: true,
			secureOptions: sslConfig.minimumTLSVersion
		};
		console.log('options', options);
		server = https.createServer(options, app);

		// set up a route to redirect http to https
		const redirServ = http.createServer(function(req, res) {
			console.log('got request');
			res.writeHead(301,{Location: `https://${req.headers.host}${req.url}`});
			res.end();

			// Or, if you don't want to automatically detect the domain name from the request header, you can hard code it:
			// res.redirect('https://example.com' + req.url);
		});

		// have it listen on 8080
		redirServ.listen(8080, '0.0.0.0');
	} else {
		server = http.createServer(app);
	}

	const port = app.get('port');

	// start the web server
	return server.listen(port, function() {
		app.emit('started');
		const baseUrl = (isHttps ? 'https://' : 'http://') + app.get('host') + ':' + app.get('port');
		console.log('Web server listening at: %s', baseUrl);
		if (app.get('loopback-component-explorer')) {
			const explorerPath = app.get('loopback-component-explorer').mountPath;
			console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
		}
	});
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
	if (err) throw err;

  // start the server if `$ node server.js`
	if (require.main === module) {
		const application = app.start();

		app.io = require('socket.io')(application);
		app.io.on('connection', socket => {
			app.io.emit('stuff', 'message');

			console.log('a user connected');

			socket.on('disconnect', () => {
				console.log('user disconnected');
			});
			socket.on('join-channel', room => {
				socket.join(room);
			});
			socket.on('leave-channel', room => {
				socket.leave(room);
			});
		});
	}
});
